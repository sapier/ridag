var MarkerTypeLookupTable = new Array();

var f = function() 
{
    function clearElement(id)
    {
        var element_to_clear = document.getElementById(id);
        
        if (!element_to_clear)
            return;
            
        while (element_to_clear.hasChildNodes()) {
            element_to_clear.removeChild(element_to_clear.lastChild);
        }
    }
    
    function loadMarkerTypeLookupTable()
    {
        var xmlHttp = new XMLHttpRequest();
        
        xmlHttp.open("GET", "/static/markertypemapping.json");
        
        xmlHttp.onreadystatechange = function() 
        {    
            if ((this.readyState == 4) && (this.status == 200))
            {
                markermappings = JSON.parse(this.responseText);
                
                for (var i = markermappings.mapping.length -1; i >= 0; i--)
                {
                    if ((markermappings.mapping[i].type) && (markermappings.mapping[i].image))
                    {
                        MarkerTypeLookupTable[markermappings.mapping[i].type] = markermappings.mapping[i].image;
                    }
                }
            }
        }
        
        xmlHttp.send( );
    }
    
    function delMarkerHandler(event)
    {
        var contextmenudiv = document.getElementById("delmarker_contextmenu");
        contextmenudiv.style.visibility = "hidden";

        var markeruid = document.getElementById("uid");

        if (!markeruid)
            return;

        if (markeruid.value < 0)
            return;

        event.preventDefault();

        var url ="formhandler";
         
        var tosend ="{" +
                    "\"cmd\" : \"DelMarker\"," +
                    "\"xCoord\" : " + xCoord + "," +
                    "\"yCoord\" : " + yCoord + "," +
                    "\"uid\" : " + markeruid.value +
                    "}";

        markeruid.value = -1;

        var xhr = new XMLHttpRequest();
        
        xhr.open("POST", url, true); 
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(tosend));
        
        loadMarkers(xCoord,yCoord);
    }
    
    function loadMarkers(x,y)
    {
        var xmlHttp = new XMLHttpRequest();
        
        xmlHttp.open("GET", "/dynamic/markers_" + x + "_" + y + ".json");
        
        xmlHttp.onreadystatechange = function() 
        {    
            if ((this.readyState == 4) && (this.status == 200))
            {
                var markerdata = JSON.parse(this.responseText);
                    
                var legend_div = document.getElementById('legend_div');
                
                if (legend_div)
                {
                    legend_div.parentNode.removeChild(legend_div);
                }
                
                var marker_div = document.getElementById('marker_div');
                
                if (marker_div)
                {
                    marker_div.parentNode.removeChild(marker_div);
                }
                
                if (markerdata.markers.length > 0)
                {
                    legend_div = document.createElement("div");
                    legend_div.id = "legend_div";
                    legend_div.className = "legend";
                
                    marker_div = document.createElement("div");
                    marker_div.id = "marker_div";
                    marker_div.className = "marker";
                    
                    document.getElementById("main").appendChild(legend_div);
                    document.getElementById("main").appendChild(marker_div);
                    
                    legend_table = document.createElement("table");
                    legend_table.id = "legend_table";
                    
                    var legendToAdd = [];
                    var legendToAddImg = [];
                    var mapimg =  document.getElementById("mapimage");
                    
                    for (var i = markerdata.markers.length -1; i >= 0; i--)
                    {
                        if (!legendToAdd.includes(markerdata.markers[i].text))
                        {
                            legendToAdd.push(markerdata.markers[i].text);
                            legendToAddImg.push(markerdata.markers[i].image);
                        }
                        
                        var marker_xpos = (markerdata.markers[i].xpos * parseInt(mapimg.width) -16) - window.pageXOffset;
                        var marker_ypos = (markerdata.markers[i].ypos * parseInt(mapimg.height) -16) - window.pageYOffset;                        
                        
                        markerimg = document.createElement("img");
                        markerimg.src = markerdata.markers[i].image;
                        markerimg.className = "marker";
                        markerimg.style.left = marker_xpos + "px";
                        markerimg.style.top = marker_ypos + "px";
                        markerimg.title = markerdata.markers[i].text;
                        markerimg.uid = markerdata.markers[i].uid;
                        
                        markerimg.addEventListener('wheel', wheelEventHandler, false);
                        markerimg.addEventListener('mousemove', moveEventHandler, false);
                        markerimg.addEventListener('contextmenu',markerContextMenuHandler, false);
                        
                        marker_div.appendChild(markerimg);
                    }
                    
                    for (var i = 0; i < legendToAdd.length; i++)
                    {
                        var row = legend_table.insertRow(0);
                        var cell = row.insertCell(0);
                        cell.innerHTML = legendToAdd[i];
                        var cell2 = row.insertCell(0);
                        var legendimg = document.createElement("img");
                        legendimg.className="legend";
                        legendimg.src = legendToAddImg[i];
                        cell2.appendChild(legendimg);
                    }
                    
                    legend_div.appendChild(legend_table);
                }
            }
        }
        
        xmlHttp.send( );
    }

    function wheelEventHandler(event)
    {
        var mapimage = document.getElementById("mapimage"); 

        var width = parseInt(mapimage.width);
        var height = parseInt(mapimage.height);

        var event_offsetX = event.offsetX;
        var event_offsetY = event.offsetY;

        if (mapimage != this)
        {
            var maprect = mapimage.getBoundingClientRect();
            var wheelrect = this.getBoundingClientRect();

            event_offsetX = (wheelrect.left + event.offsetX) - maprect.left;
            event_offsetY = (wheelrect.top + event.offsetY) - maprect.top;
        }

        var zoom_factor = 1.1;
        
        if(event.deltaY > 0) 
        {
            zoom_factor = 0.9;
        }
        
        var new_height = height * zoom_factor;
        
        if (new_height < window.innerHeight)
        {
            new_height = window.innerHeight;
            zoom_factor = new_height/height;
        }
        var new_width = width * zoom_factor;
               
        var relative_mouse_x_position = event_offsetX / width;
        var relative_mouse_y_position = event_offsetY / height;

        mapimage.style.width = new_width + "px";
        mapimage.style.height = new_height + "px";
        
        window.scrollBy((new_width - width) * relative_mouse_x_position, (new_height - height) * relative_mouse_y_position);
        event.preventDefault();

        loadMarkers(xCoord,yCoord);
        
        var contextmenudiv = document.getElementById("marker_contextmenu");
        contextmenudiv.style.visibility = "hidden";
      
        var delmarkermenudiv = document.getElementById("delmarker_contextmenu");
        delmarkermenudiv.style.visibility = "hidden";
    }
    
    function moveEventHandler(event)
    {
        var current_buttons = event.buttons;
        
        if (current_buttons == 1)
        {
            window.scrollBy(-event.movementX, -event.movementY);
            loadMarkers(xCoord,yCoord);
            
            var contextmenudiv = document.getElementById("marker_contextmenu");
            contextmenudiv.style.visibility = "hidden";

            var delmarkermenudiv = document.getElementById("delmarker_contextmenu");
            delmarkermenudiv.style.visibility = "hidden";
        }
    }
    
    function mapMouseDownEventHandler(event)
    {
        event.preventDefault();
        event.stopPropagation();
        
        var contextmenudiv = document.getElementById("marker_contextmenu");
        contextmenudiv.style.visibility = "hidden";

        var delmarkermenudiv = document.getElementById("delmarker_contextmenu");
        delmarkermenudiv.style.visibility = "hidden";
    }
    
    function updateAddMarkerIcon()
    {
        var typeinput = document.getElementById("markertype");
        
        var markericon = document.getElementById("markericon");
        
        if (MarkerTypeLookupTable[typeinput.value])
        {
            markericon.src = MarkerTypeLookupTable[typeinput.value];
        }
        else {
            console.log("No mapping found for: " + typeinput.value);
            markericon.src = "/static/crossmarker.png";
        }
    }
    
    function mapContextMenuHandler(event)
    {
        event.preventDefault();
        
        var contextmenudiv = document.getElementById("marker_contextmenu");
        

        contextmenudiv.style.left = event.clientX + 30;
        contextmenudiv.style.top = event.clientY + 30;
        contextmenudiv.style.visibility = "visible";
        contextmenudiv.className="addmarkermenu";
        
        var form_markerx = document.getElementById("markerx");
        var form_markery = document.getElementById("markery");
        
        updateAddMarkerIcon();
        
        var width = parseInt(window.getComputedStyle(this).width);
        var height = parseInt(window.getComputedStyle(this).height);
        
        var left = parseInt(window.getComputedStyle(this).left);
        var top = parseInt(window.getComputedStyle(this).top);

        form_markerx.value = (event.clientX + window.pageXOffset) / width;
        form_markery.value = (event.clientY + window.pageYOffset) / height;
    }

    function markerContextMenuHandler(event)
    {
        event.preventDefault();

        var contextmenudiv = document.getElementById("delmarker_contextmenu");
        
        contextmenudiv.style.left = event.clientX + 30;
        contextmenudiv.style.top = event.clientY + 30;
        contextmenudiv.style.visibility = "visible";
        contextmenudiv.className = "delmarkermenu";

        var markertitle = document.getElementById("markertitle");
        markertitle.innerHTML = this.title;

        var markeruid = document.getElementById("uid");
        markeruid.value = this.uid;
    }
    
    function saveMarkerHandler()
    {
        var contextmenudiv = document.getElementById("marker_contextmenu");
        contextmenudiv.style.visibility = "hidden";
        
        var typeinput = document.getElementById("markertype");
        var textinput = document.getElementById("markertext");
        var markerxinput = document.getElementById("markerx");
        var markeryinput = document.getElementById("markery");
        
        var url ="formhandler";
        
        var tosend ="{" +
                    "\"cmd\" : \"AddMarker\"," +
                    "\"xCoord\" : " + xCoord + "," +
                    "\"yCoord\" : " + yCoord + "," +
                    "\"xMarkerPos\" : " + markerxinput.value + "," +
                    "\"yMarkerPos\" : " + markeryinput.value + "," +
                    "\"markertype\" : \"" + typeinput.value + "\"," +
                    "\"uid\" : \"" + Date.now() + "\"";
        
        if (textinput.value != "")
        {
            tosend += ",\"markertext\" : \"" + textinput.value + "\"";
        }
        
        tosend += "}"
        
        var xhr = new XMLHttpRequest();
        
        xhr.open("POST", url, true); 
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(tosend));
        
        loadMarkers(xCoord,yCoord);
    }
    
    function clickMinimizeHandler(event)
    {
        if (navPlaneVisible)
        { navPlaneVisible = false; }
        else
        { navPlaneVisible = true; }
        
        triggerDrawNavPlane();
    }
    
    function drawNavPlane(statuspage)
    {
        var navplane = document.getElementById("mapnavig");
        
        var tbl = document.getElementById('navtable');
        
        if (tbl)
        {
            tbl.parentNode.removeChild(tbl);
        }
        
        var gridwidth = 30;
        var gridheight = gridwidth;
    
        var maxX = statuspage["dimension-X"];
        var maxY = statuspage["dimension-Y"];
        var icon = "/static/icon_minimize.png";
        
        if (!navPlaneVisible)
        {
            maxX = 0;
            maxY = 0;
            icon = "/static/icon_expand.png";
        }
            
        navplane.style.visibility = "visible";
        navplane.className = "navplane"

        navplane.style.width =  ((maxX + 1) * gridwidth  + (maxX + 1)) + "px";
        navplane.style.height = ((maxY + 1) * gridheight  + (maxY + 1)) + "px";
    
        tbl = document.createElement("table");
        tbl.id = "navtable";
        tbl.className = "navplane";
    
        var coords = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
        for (var y = 0; y <= maxY; y++)
        {
            var row = document.createElement("tr");
            row.style.border = "1px solid black";
            
            for (var x = 0; x <= maxX; x++)
            {
                var cell;
                if (y == 0) {
                    cell = document.createElement("th");
                    if (x > 0)
                    {
                        cell.innerHTML = coords.charAt(x-1);
                    }
                    else
                    {
                        icon_img = document.createElement("img");
                        icon_img.src = icon;
                        icon_img.className = "navplaneicon";
                        cell.appendChild(icon_img);
                        icon_img.addEventListener('click', clickMinimizeHandler, false);
                    }
                }
                else {
                    if (x == 0)
                    {
                        cell = document.createElement("th");
                        cell.innerHTML = y;
                    }
                    else {
                        cell = document.createElement("td");
                        cell.innerHTML = "<a href=\"/webmap_" + coords.charAt(x-1) + "_" + y + "\">" +
                            "<img src=\"CellImg_" + (x-1) + "-" + (y-1) + ".jpg\" width = \"" + gridwidth + "px\" height = \"" + gridheight + "px\">" +
                            "</a>";
                    }
                }
                cell.className = "navplane";
                cell.style.width = gridwidth + "px";
                cell.style.height = gridheight + "px";
                row.appendChild(cell);
            }
            tbl.appendChild(row);
        }
        navplane.appendChild(tbl);
    }

    function triggerDrawNavPlane()
    {

        var xmlHttp = new XMLHttpRequest();
        
        xmlHttp.open("GET", "/dynamic/gridstatus.json");
        
        xmlHttp.onreadystatechange = function() 
        {    
            if ((this.readyState == 4) && (this.status == 200))
            {
                var statuspage = JSON.parse(this.responseText);
                drawNavPlane(statuspage);
            }
        }

        xmlHttp.send( );
    }
   
    var mapimg =  document.getElementById("mapimage"); 
    
    mapimg.addEventListener('wheel',wheelEventHandler,false);
    mapimg.addEventListener('mousemove', moveEventHandler, false);
    mapimg.addEventListener('mousedown', mapMouseDownEventHandler, false);
    mapimg.addEventListener('contextmenu', mapContextMenuHandler, false);
    
    mapimg.width = window.innerHeight;
    mapimg.height = window.innerHeight;
    
    loadMarkers(xCoord,yCoord);
    
    var navPlaneVisible = true;
    
    var contextmenudiv = document.getElementById("marker_contextmenu");
    contextmenudiv.style.visibility = "hidden";

    var delmarkermenudiv = document.getElementById("delmarker_contextmenu");
    delmarkermenudiv.style.visibility = "hidden";
    
    var markersavebtn = document.getElementById("saveMarker");
    markersavebtn.addEventListener('click', saveMarkerHandler, false);

    var markerdelbtn = document.getElementById("delMarker");
    markerdelbtn.addEventListener('click', delMarkerHandler, false);
    
    var markertypeselect = document.getElementById("markertype");
    markertypeselect.addEventListener('change', updateAddMarkerIcon, false);
    
    loadMarkerTypeLookupTable();

    triggerDrawNavPlane();
};

window.addEventListener('load', f, false);
