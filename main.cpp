/*
 * main.cpp
 *
 *  Created on: 25.01.2019
 *      Author: sapier
 */
#include "switches.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <signal.h>

#ifdef HAVE_RAW_SOCKET_SUPPORT
#include <sys/socket.h>
#include <netinet/in.h>
#include <linux/if_ether.h>
#include <netinet/if_ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#endif

#ifdef HAVE_INTERNAL_WEBSERVER
#include "webserver.hpp"
#endif

#include <ctime>

#include "ServerDummy.hpp"
#include "RealServer.hpp"
#include "manager.h"

#define SERVERGRIDFILE "/srv/atlas/ShooterGame/ServerGrid.json"

#define DEFAULT_MAXACTIVESERVERS 3
#define DEFAULT_INACTIVITYTIMEOUT 300
#define DEFAULT_INTERNALWEBSERVERPORT 9200

#define DEFAULT_REDISIP "127.0.0.1"
#define DEFAULT_REDISPORT 6379
#define DEFAULT_REDISPWD "foobared"

struct configparams
{
	std::string redisip;
	std::string redispwd;
	uint16_t redisport;
	uint16_t inernalwebserverport;
	unsigned int inactivitytimeout;
	unsigned int maxactiveservers;
	char* servergridfile;
	bool  isDummy;
};

static struct option long_options[] =
{
		{ "servergrid", required_argument, NULL, 'c' },
		{ "webport", required_argument, NULL, 'w' },
		{ "redisip", required_argument, NULL, 'r' },
		{ "redispasswd", required_argument, NULL, 's' },
		{ "redisport", required_argument, NULL, 'p' },
		{ "maxactiveservers", required_argument, NULL, 'm' },
		{ "inactivitytimeout", required_argument, NULL, 'i' },
		{ "dummy", no_argument, NULL, 'd' },
		{ 0, 0, 0, 0 }
};


static bool glb_keeprunning = true;

/*****************************************************************************/
void sigIntHandler(int)
{
	glb_keeprunning = false;
}

/*****************************************************************************/
void hexdump(char* buffer, size_t size)
{
        for (unsigned int i = 0; i < (size/16) +1; i++)
        {
                printf("%04x:", i);
                for (unsigned j = 0; (j < 16) && (i*16 + j < size); j++)
                {
                        printf(" %02x", buffer[i*16+j] & 0xFF);
                }
                printf("\n");
        }
}

/*****************************************************************************/
void printUsage()
{
	printf("Parameters:\n");
	printf("-c/--servergrid        json config for atlas servergrid (mandatory)\n");
	printf("-w/--webport           port to be used for status and cellimage webserver\n"
		   "                       (default: 9200)\n");
	printf("-r/--redisip           ip address used to connect to redis (default: 127.0.0.1)\n");
	printf("-s/--redispasswd       password for connection to redis (default: foobared)\n");
	printf("-p/--redisport         port to use to connect to redis (default: 6379)\n");
	printf("-m/--maxactiveservers  maximum number of atlas servers to start (default: 3)\n");
	printf("-i/--inactivitytimeout time in seconds to keep a server active after last\n"
		   "                       player left (default: 300)\n");
	printf("-d/--dummy             don't start any atlas server even if it should be started\n");
}

#ifdef HAVE_RAW_SOCKET_SUPPORT
/*****************************************************************************/
int getPort(const char* buffer, size_t size)
{
	struct ether_header* eth_hdr = (struct ether_header*) buffer;

	if (ntohs(eth_hdr->ether_type) != 0x0800)
		return -1;

	struct ip* ip_hdr = (struct ip*) &buffer[sizeof(struct ether_header)];

	if ((ip_hdr->ip_v != 4) || (ip_hdr->ip_p != 6))
		return -1;

	struct tcphdr* tcp_hdr = (struct tcphdr*) &buffer[sizeof(struct ether_header) + sizeof(struct ip)];

	return ntohs(tcp_hdr->th_dport);
}
#endif /* HAVE_RAW_SOCKET_SUPPORT */

/*****************************************************************************/
void parse_commandline(configparams& config, int argc, char** argv)
{
	int ch;
	while( (ch = getopt_long(argc, argv, "c:w:r:s:p:m:i:d", long_options, NULL)) != -1)
	{

		switch (ch)
		{
		case 'c':
			config.servergridfile = optarg;
			break;

		case 'w':
			config.inernalwebserverport = std::stod(optarg);
			break;

		case 'r':
			config.redisip = optarg;
			break;

		case 's':
			config.redispwd = optarg;
			break;

		case 'p':
			config.redisport = std::stod(optarg);
			break;

		case 'm':
			config.maxactiveservers = std::stod(optarg);
			break;

		case 'i':
			config.inactivitytimeout = std::stod(optarg);
			break;

		case 'd':
			config.isDummy = true;
			std::cout << "/***********************************************************/" << std::endl;
			std::cout << "/*                                                         */" << std::endl;
			std::cout << "/* ATTENTION: dummy mode enabled, servers WONT be started! */" << std::endl;
			std::cout << "/*                                                         */" << std::endl;
			std::cout << "/***********************************************************/" << std::endl;
			break;

		default:
			std::cout << "invalid parameters" << std::endl;
			printUsage();
			exit(-1);
		}
	}
}

/*****************************************************************************/
int main(int argc, char** argv)
{

	/* intitial application sanity checks */
	uid_t ownuid = geteuid();

	if (ownuid == 0)
	{
		std::cout << "This program shall not be run as root!" << std::endl;
		exit(-1);
	}

	configparams myconfig = {
			DEFAULT_REDISIP,
			DEFAULT_REDISPWD,
			DEFAULT_REDISPORT,
			DEFAULT_INTERNALWEBSERVERPORT,
			DEFAULT_INACTIVITYTIMEOUT,
			DEFAULT_MAXACTIVESERVERS,
			0
	};

	parse_commandline(myconfig, argc, argv);

	if (myconfig.servergridfile == 0)
	{
		std::cerr << "No servergridfile specified!" << std::endl;
		printUsage();
		exit(-1);
	}


	/* todo read path of servergridfile from command line or config file */
	manager* servermanager = new manager(myconfig.servergridfile, myconfig.redisip.c_str(), myconfig.redisport, myconfig.redispwd.c_str(), myconfig.isDummy);


	if (!servermanager->isValid())
	{
		std::cerr << "Failed to initialize servergrid manager" << std::endl;
		exit(-1);
	}

	servermanager->setMaxActiveServers(myconfig.maxactiveservers);
	servermanager->setInactivityTimeout(myconfig.inactivitytimeout);

	printf("Atlas singleplayer servermanager\n");
	printf("\tServergridfile:     %s\n", myconfig.servergridfile);
	printf("\tMax active servers: %d\n", myconfig.maxactiveservers);

	printf("\tX cluster count:    %d\n", servermanager->getMaxX());
	printf("\tY cluster count:    %d\n", servermanager->getMaxY());
	printf("\tServername:         %s\n", servermanager->getServerName().c_str());
	printf("\tAtlasid:            %s\n", servermanager->getAtlasId().c_str());
	printf("\tGrid:\n");

	for (unsigned int x = 0; x < servermanager->getMaxX(); x++)
	{
		for (unsigned int y = 0; y < servermanager->getMaxY(); y++)
		{
			printf("\tX=% 2d Y=% 2d: isHomeServer=%s\n", x, y, servermanager->isHomeServer(x,y) ? "true" : "false");
		}
	}

#ifdef HAVE_RAW_SOCKET_SUPPORT
	int dummyfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));

	if (dummyfd < 0)
	{
		printf("Failed to open dummy listener socket\n");
		exit(-1);
	}
#endif /* HAVE_RAW_SOCKET_SUPPORT */

#ifdef HAVE_INTERNAL_WEBSERVER
		/* initialize the webserver */
		WebServer::getInstance();

		struct MHD_Daemon* webserver = MHD_start_daemon( MHD_USE_DEBUG, myconfig.inernalwebserverport, 0, 0,
														 &answer_to_connection, servermanager, MHD_OPTION_END);

		if (webserver == 0)
		{
			printf("Failed to initialize libmicrohttpd webserver\n");
			exit(-1);
		}

#endif /* HAVE_INTERNAL_WEBSERVER */

	// initial update of serverlist
	servermanager->checkServerList();

#ifdef PRINT_STATUS_TO_CONSOLE
	std::time_t lastPrinted = std::time(nullptr);
#endif /* PRINT_STATUS_TO_CONSOLE */

	signal(SIGINT, sigIntHandler);

	while(glb_keeprunning)
	{
		/* listen do raw socket */
		fd_set rfds;
		fd_set wfds;
		fd_set efds;
		struct timeval tv;
		tv.tv_sec = 1;
		tv.tv_usec = 0;

		int maxfd = 0;
		FD_ZERO(&rfds);
		FD_ZERO(&wfds);
		FD_ZERO(&efds);

#ifdef HAVE_RAW_SOCKET_SUPPORT
		FD_SET(dummyfd, &rfds);
		maxfd = dummyfd;
#endif /* HAVE_RAW_SOCKET_SUPPORT */

#ifdef HAVE_INTERNAL_WEBSERVER
		int httpdmaxfd = 0;
		if (MHD_get_fdset (webserver, &rfds, &wfds, &efds, &httpdmaxfd) != MHD_YES)
		{
			printf("Failed to get fds for libmicrohttpd");
		}
		else {
			maxfd = std::max(maxfd, httpdmaxfd);
		}
#endif /* HAVE_INTERNAL_WEBSERVER */


		int retval = select( maxfd+1, &rfds, &wfds, &efds, &tv);

		if (retval == -1)
		{
			if (errno != EINTR)
			{
				perror("select()");
				exit(-1);
			}
			continue;
		}

#ifdef HAVE_RAW_SOCKET_SUPPORT
		if (FD_ISSET(dummyfd, &rfds))
		{
			char buffer[2048];
			memset(buffer, 0, sizeof(buffer));
			size_t revcsize = recvfrom(dummyfd , buffer , sizeof(buffer) , 0 , 0, 0);


			int port = getPort(buffer, revcsize);

			if (port > 0)
				servermanager->frameForPort(port);
		}
#endif /* HAVE_RAW_SOCKET_SUPPORT */

#ifdef HAVE_INTERNAL_WEBSERVER
		MHD_run_from_select (webserver, &rfds, &wfds, &efds);
#endif /* HAVE_INTERNAL_WEBSERVER */

		servermanager->checkServerList();

#ifdef PRINT_STATUS_TO_CONSOLE
		if (std::time(nullptr) - lastPrinted < 10)
			continue;

		lastPrinted = std::time(nullptr);

		printf("     |     |      | isHome- |     last    | Players |      Last    |         |\n");
		printf("  X  |  Y  | Type | server  |    Alive    | on/total|     online   | trigger | rsn\n");
		printf("-----+-----+------+---------+-------------+---------+--------------+---------|----\n");

		for (unsigned int x = 0; x < servermanager->getMaxX(); x++)
		{
			for (unsigned int y = 0; y < servermanager->getMaxY(); y++)
			{
				printf(" % 3d | % 3d |% 6s|   % 3s   | % 10ds | % 3d/% 3d | % 11ds | % 3s(%1d)  | %2d\n",
						x, y,
						servermanager->getType(x,y) == DummyListener ? "SPACER" :
								servermanager->getType(x,y) == AtlasServer ? "ATLAS" : "UNDEF",
						servermanager->isHomeServer(x,y) ? "yes" : "no",
						servermanager->timeSinceLastOnline(x,y),
						servermanager->getActiveUsers(x,y),servermanager->getPlayerCount(x,y),
						servermanager->timeSinceLastPlayerOnline(x,y),
						servermanager->isTriggering(x,y) ? "yes" : "no",
						servermanager->getFreeServerSlots() > 0 ? 1 : 0,
						servermanager->isSupposedToRun(x, y)
						);
			}
		}
#endif /* PRINT_STATUS_TO_CONSOLE */
	}

	printf("Cleaning up...");
	delete servermanager;
	printf("done\n");

	return 0;
}
