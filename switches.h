/*
 * switches.h
 *
 *  Created on: 02.02.2019
 *      Author: sapier
 */

#ifndef SWITCHES_H_
#define SWITCHES_H_

#define APPNAME "RIDAG (Resource Inexpensive Dynamic Atlas Grid)"
#define APPVERSION "0.0.1"

#undef HAVE_RAW_SOCKET_SUPPORT
#define HAVE_INTERNAL_WEBSERVER
#undef PRINT_STATUS_TO_CONSOLE

#define USE_WINE


#endif /* SWITCHES_H_ */
