/*
 * iserver.hpp
 *
 *  Created on: 25.01.2019
 *      Author: sapier
 */

#ifndef ISERVER_HPP_
#define ISERVER_HPP_

typedef enum {
	Undefined,
	DummyListener,
	AtlasServer
} ServerType;

class IServer
{

public:
	IServer(ServerType type = Undefined) :
		m_type(type),
		m_valid(false),
		m_gridX(-1),
		m_gridY(-1)
	{}

	virtual ~IServer() {};

	virtual int getPlayerCount() = 0;

	ServerType getType()
	{ return m_type; }

	int getX()
	{ return m_gridX; }

	int getY()
	{ return m_gridY; }

	bool isValid()
	{ return m_valid; }

protected:
	ServerType m_type;
	bool m_valid;

	int m_gridX;
	int m_gridY;
};






#endif /* ISERVER_HPP_ */
