/*
 * common.hpp
 *
 *  Created on: 27.01.2019
 *      Author: sapier
 */

#ifndef COMMON_HPP_
#define COMMON_HPP_

#include <hiredis/hiredis.h>

/**
 *
 * @param context
 * @param portnumber
 * @return
 */
std::string getClusterByPort(redisContext* context, int portnumber);

/**
 *
 * @param context
 * @param clusterid
 * @return
 */
int readPlayerCount(redisContext* context, std::string clusterid);

/**
 *
 * @param context
 * @param atlasid
 * @param clusterid
 * @return
 */
int readTimeSinceClusterLastOnline(redisContext* context, std::string atlasid, std::string clusterid);

/**
 *
 * @param tofix
 * @param toreplace
 * @param replaceby
 */
void findAndReplace(std::string &tofix, std::string toreplace, std::string replaceby);

#define DBG(x) x


#endif /* COMMON_HPP_ */
