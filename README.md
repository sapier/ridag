RIDAG
=====
Resource Inexpensive Dynamic Atlas Grid

## What is RIDAG
RIDAG is a addon software to the mmog atlas. Atlas (in its current state) doesn't have a single player mode and does require all clusters of a Grid to be online all the time. The purpose of RIDAG is to provide a large grid even to players that don't have the resources to do this.

## How does it work?
RIDAG basically monitors the grid and dynamically starts or stops grid clusters as required by the current players. A user may specify the maximum amount of concurrent active clusters. RIDAG uses the same servergrid.json which is used by the atlas servers. After parsing that file RIDAG will start the clusters based on certain requirements, e.g. homeservers, clusters with active players ..). If a cluster no longer is in use by a player it uses the rcon interface to save the clusters map and shut it down till it's required once again. RIDAG monitors the redis db to detect if a cluster is required to online. As soon as this is detected (and if there are open slots left) the cluster will be started.

## Who should use it?
How much resources (RAM/CPU) is saved depends on how many grids are used at a single time. For example if you have four players on your 2x2 grid and everyone is in a different one you wont save anything. The opposite situation is if you have a single player only and a 16x16 grid. In this situation you will (usually) have only one or maybe two clusters online (additional to homeserver clusters of course).
So typical scenarios where RIDAG makes sense:
* Single player 
* Single Team
* Large grids with low player count

## Limitations
* As of February 2019 linux version of atlas is basically useless thus wine64 is used to run the servers. 
* Homeservers have to be always online as (atm) there's no way to tell steam lobby to show them online if the atlas server ain't running.
* Clusters with offline players need to be online for same reason as the homeservers (steam lobby simply won't connect to offline servers)
* transition from an active grid to an former inactive grid may take up to a few minutes (depends on your server HW)
* if you have more players then maximum active clusters there's a chance for a player to get stuck in a inactive cluster. No data will be lost but it requires manual actions to free that player. E.g. make other players free a cluster so the forced inactive cluster can become active.
* inactive servers may not progress in time (not verified)

## Additional Features
* RIDAG contains a small webpage showing the current state of the grid
* As there's already a webserver this webpage is able to provide the MapImg and CellImg files for the ingame map too.

## Dependecies
* jsoncpp https://github.com/open-source-parsers/jsoncpp
* libmicrohttpd https://www.gnu.org/software/libmicrohttpd/
* hiredis https://github.com/redis/hiredis
* wine 4.0 (may work with lower version but untested)

## License
If you use this code to provide any commercial service you are required to publish the whole corresponding source code used e.g. by providing a download link.
As long as they don't interfere with upper requirement all other conditions of GPLv2 shall be used. 


## Tested environment (default settings)
 * Debian buster (20190202)
 * wine 4.0rc6
 * Atlas 17.1
 * 4x4 grid
 * Virtual machine, 4 core, 13GB (Host: Phenom II x6 16GB)
