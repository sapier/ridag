/*
 * webserver.cpp
 *
 *  Created on: 02.02.2019
 *      Author: sapier
 */
#include "webserver.hpp"

#ifdef HAVE_INTERNAL_WEBSERVER
#include <microhttpd.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <limits.h>
#include <stdio.h>
#include <string.h>

#include "manager.h"
#include "common.hpp"

const char* file_not_found_404 ="<html><body>Not found!</body></html>";
const char* file_ok_200 = "<html><body>ok</body></html>";

const char* filename_markertypemapping = "static/markertypemapping.json";


/*****************************************************************************/
int answer_to_connection (void *cls, struct MHD_Connection *connection,
                          const char *url,
                          const char *method, const char *version,
                          const char *upload_data,
                          size_t *upload_data_size, void **con_cls)
{
	WebServer* instance = WebServer::getInstance();

	if (instance == 0)
		return MHD_NO;

	return instance->answerToConnection(cls, connection, url, method, version,
			                            upload_data, upload_data_size, con_cls);
}



/*****************************************************************************/
WebServer::WebServer()
{
	loadMarkerMappings();
}

/*****************************************************************************/
void WebServer::loadMarkerMappings()
{
	FILE* MARKERMAPPING = fopen(filename_markertypemapping, "r");

	if (MARKERMAPPING == 0)
	{
		DBG(std::cout << "WebServer::WebServer() failed to open marker file" << filename_markertypemapping << std::endl;)
		return;
	}

	std::string mappingfile("");
	char readbuffer[1024];
	memset(readbuffer,0, sizeof(readbuffer));
	int readlen = 0;
	while((readlen = fread(readbuffer, 1, sizeof(readbuffer), MARKERMAPPING)) > 0)
	{
		mappingfile.append(std::string(readbuffer, readlen));
		memset(readbuffer, 0, sizeof(readbuffer));
	}

	fclose(MARKERMAPPING);

	Json::Value root;
	Json::Reader reader;
	if (!reader.parse(mappingfile, root))
	{
		DBG(std::cout << "loadMarkerMappings: failed to parse json mapping file" << std::endl;)
		return;
	}

	if (root.empty())
	{
		DBG(std::cout << "loadMarkerMappings: didn't get valid json" << std::endl;)
		return;
	}

	Json::Value mappings = root.get("mapping", 0);

	DBG(std::cout << "loadMarkerMappings: evaluating " << mappings.size() << " lines in mapping table" << std::endl;)

	for (unsigned int i = 0; i < mappings.size(); i++)
	{
		std::string type = mappings[i].get("type", "").asString();
		std::string image = mappings[i].get("image", "").asString();

		if (( type.size() == 0 ) || ( image.size() == 0 ))
			continue;

		if (m_markerTypeImageLookupTable.find(type) != m_markerTypeImageLookupTable.end())
		{
			DBG(std::cout << "loadMarkerMappings: type " << type << "has multiple mappings!" << std::endl;)
			continue;
		}

		m_markerTypeImageLookupTable[type] = image;
	}
	DBG(std::cout << "loadMarkerMappings: loaded " << m_markerTypeImageLookupTable.size() << " type to image mappings" << std::endl;)
}

/*****************************************************************************/
WebServer* WebServer::getInstance()
{
	static WebServer* s_instance = 0;

	if (!s_instance) {
		s_instance = new WebServer();
	}

	return s_instance;
}

/*****************************************************************************/
void WebServer::pageHandlerServerGridImages(const char* url, std::stringstream& outbuffer,
		manager* servermanager)
{
	printf("trying to get file %s\n", url);
	char buffer[PATH_MAX];
	memset(buffer, 0, sizeof(buffer));
	snprintf(buffer, sizeof(buffer), "%s/ShooterGame/ServerGrid%s", servermanager->getServerDir().c_str(), url);
	char buffer_realpath[PATH_MAX];
	memset(buffer_realpath, 0, sizeof(buffer));

	realpath(buffer, buffer_realpath);
	printf("Requested: %s Realpath: %s\n", buffer, buffer_realpath);

	if (strncmp(buffer_realpath, servermanager->getServerDir().c_str(), servermanager->getServerDir().size()) != 0)
	{
		return;
	}

	std::ifstream toprovide(buffer_realpath);

	if (toprovide)
	{
		outbuffer << toprovide.rdbuf();
		toprovide.close();
	}
}

/*****************************************************************************/
void WebServer::pageHandlerStaticFiles(const char* url, std::stringstream& outbuffer)
{
	char buffer[PATH_MAX];
	memset(buffer, 0, sizeof(buffer));
	snprintf(buffer, sizeof(buffer), "./%s", url);
	char buffer_realpath[PATH_MAX];
	char buffer_cwd[PATH_MAX];
	memset(buffer_realpath, 0, sizeof(buffer));
	memset(buffer_cwd, 0, sizeof(buffer));

	realpath(buffer, buffer_realpath);
	getcwd(buffer_cwd, sizeof(buffer_cwd));
	if (strncmp(buffer_realpath, buffer_cwd, strlen(buffer_cwd)) != 0)
	{
		return;
	}

	std::ifstream toprovide(buffer_realpath);

	if (toprovide)
	{
		outbuffer << toprovide.rdbuf();
		toprovide.close();
	}
}

/*****************************************************************************/
void WebServer::pageHandlerDynamicFiles(const char* url, std::stringstream& outbuffer,
		manager* servermanager)
{
	char buffer[PATH_MAX];
	memset(buffer, 0, sizeof(buffer));
	snprintf(buffer, sizeof(buffer), "./%s", url);
	char buffer_realpath[PATH_MAX];
	char buffer_cwd[PATH_MAX];
	memset(buffer_realpath, 0, sizeof(buffer));
	memset(buffer_cwd, 0, sizeof(buffer));

	realpath(buffer, buffer_realpath);
	getcwd(buffer_cwd, sizeof(buffer_cwd));
	if (strncmp(buffer_realpath, buffer_cwd, strlen(buffer_cwd)) != 0)
	{
		return;
	}

	if (strncmp(url, "/dynamic/gridstatus.json", strlen("/dynamic/gridstatus.json")) == 0)
	{
		pageHandlerStatusJson(outbuffer, servermanager );
	}

	std::ifstream toprovide(buffer_realpath);

	if (toprovide)
	{
		outbuffer << toprovide.rdbuf();
		toprovide.close();
	}
}

/*****************************************************************************/
void WebServer::pageHandlerWebMap(const char* url, std::stringstream& outbuffer)
{

	if (strlen(url) < 11)
		return;

	int y_coordinate = -1;
	char lexical_x = url[8];
	try {
		y_coordinate = std::stod(&url[10]) -1;
	} catch(const std::invalid_argument& ia)
	{
		printf("getWebmap can't convert \"%s\"\n", &url[10]);
		return;
	}
	int x_coordinate = lexical_x - 65;

	outbuffer << "<html>" << std::endl;
	outbuffer << "    <head>" << std::endl;
	outbuffer << "        <meta charset=\"utf-8\">" << std::endl;
	outbuffer << "        <link href=\"static/style.css\" rel=\"stylesheet\" media=\"all\">" << std::endl;
	outbuffer << "        <script src=\"static/map.js\"></script>" << std::endl;
	outbuffer << "        <script type = \"text/javascript\">var xCoord = " << x_coordinate << "; var yCoord = " << y_coordinate << ";</script>" << std::endl;
	outbuffer << "        <title>Atlas Cluster "<< lexical_x << (y_coordinate+1) << "</title>" << std::endl;
	outbuffer << "    </head>" << std::endl;
	outbuffer << "    <body>" << std::endl;
	outbuffer << "        <div id=\"main\">" << std::endl;
	outbuffer << "            <img class=\"mapimage\" id=\"mapimage\" src=\"/CellImg_" << x_coordinate << "-" << y_coordinate << ".jpg\">" << std::endl;
	outbuffer << "        </div>" << std::endl;
	outbuffer << "        <div id=\"marker_contextmenu\">" << std::endl;
	outbuffer << "        <font size=-1>Add marker:</font><br>" << std::endl;
	outbuffer << "        <table width=\"100%\">" << std::endl;
	outbuffer << "        <tr><td>" << std::endl;
	outbuffer << "            <select name=\"markertype\" id=\"markertype\">" << std::endl;


	for (auto iter = m_markerTypeImageLookupTable.begin();
			iter != m_markerTypeImageLookupTable.end(); iter++)
	{
		outbuffer << "                <option value=\"" << iter->first << "\">" << iter->first << "</option>" << std::endl;
	}

	outbuffer << "            </select>" << std::endl;
	outbuffer << "            <input type=\"hidden\" name=\"markerx\" id=\"markerx\" value=\"-1\">" << std::endl;
	outbuffer << "            <input type=\"hidden\" name=\"markery\" id=\"markery\" value=\"-1\">" << std::endl;
	outbuffer << "            </td>" << std::endl;
	outbuffer << "            <td><img src=\"static/crossmarker.png\" id=\"markericon\" width=32 height=32></td>" << std::endl;
	outbuffer << "            <td><input type=\"text\" name=\"markertext\" id=\"markertext\" size=\"40\"></td></tr>" << std::endl;
	outbuffer << "        <tr><td colspan=3 align=\"right\"><input type=\"button\" id=\"saveMarker\" value=\"save\"></td></tr>" << std::endl;
	outbuffer << "        </table>" << std::endl;
	outbuffer << "        </div>" << std::endl;
	outbuffer << "        <div id=\"delmarker_contextmenu\">" << std::endl;
	outbuffer << "        <font size=-1>Delete marker:</font><br>" << std::endl;
	outbuffer << "        <table width=\"100%\">" << std::endl;
	outbuffer << "        <tr><td id=\"markertitle\">nomarker</td></tr>" << std::endl;
	outbuffer << "        <tr><td colspan=3 align=\"right\"><input type=\"hidden\" name=\"uid\" id=\"uid\" value=\"-1\"><input type=\"button\" id=\"delMarker\" value=\"delete\"></td></tr>" << std::endl;
	outbuffer << "        </table>" << std::endl;
	outbuffer << "        </div>" << std::endl;
	outbuffer << "        <div id=\"mapnavig\"></div>" << std::endl;
	outbuffer << "    </body>" << std::endl;
	outbuffer << "</html>" << std::endl;
}

/*****************************************************************************/
void WebServer::pageHandlerStatusJson(std::stringstream& outbuffer,
		manager* servermanager)
{
	outbuffer << "{";
	outbuffer << "\"gridname\": \"" << servermanager->getServerName() << "\",";
	outbuffer << "\"atlasid\": \"" << servermanager->getAtlasId() << "\",";
	outbuffer << "\"dimension-X\":" << servermanager->getMaxX() << ",";
	outbuffer << "\"dimension-Y\":" << servermanager->getMaxY() << ",";
	outbuffer << "\"serverSlotsAvailable\":" << servermanager->getFreeServerSlots() << ",";
	outbuffer << "\"grids\":[";

	for (unsigned int x = 0; x < servermanager->getMaxX(); x++)
		for (unsigned int y = 0; y < servermanager->getMaxY(); y++)
	{
			outbuffer << "{";
			outbuffer << "\"x\": " << x << ",";
			outbuffer << "\"y\": " << y << ",";
			outbuffer << "\"type\": " << servermanager->getType(x, y) << ",";
			outbuffer << "\"runreason\":" << servermanager->getType(x, y) << ",";
			outbuffer << "\"isHomeserver\": \"" << (servermanager->isHomeServer(x, y) ? "yes" : "no") << "\",";
			outbuffer << "\"timeSinceLastOnline\": " << servermanager->timeSinceLastOnline(x, y) << ",";
			outbuffer << "\"activeUsers\": " << servermanager->getActiveUsers(x, y) << ",";
			outbuffer << "\"totalPlayers\": " << servermanager->getPlayerCount(x, y) << ",";
			outbuffer << "\"timeSinceLastPlayerOnline\": " << servermanager->timeSinceLastPlayerOnline(x, y);

			if (servermanager->getType(x, y) == AtlasServer)
			{
				outbuffer << ",";
				outbuffer << "\"cpuusage\":" << servermanager->getCpuUsage(x,y) << ",";
				outbuffer << "\"memoryusage\":" << servermanager->getMemoryUsage(x,y);
			}

			outbuffer << "}";

			if ((x + 1 < servermanager->getMaxX()) ||
					(y + 1 < servermanager->getMaxY()))
			{
				outbuffer << ",";
			}
	}
	outbuffer << "]";
	outbuffer << "}";
}


/*****************************************************************************/
void WebServer::pageHandlerStatusOld(std::stringstream& outbuffer,
		manager* servermanager)
{
	outbuffer << "<html>" << std::endl;
	outbuffer << "  <head>" << std::endl;
	outbuffer << "    <title>" << APPNAME << " " << APPVERSION << "</title>" << std::endl;
	outbuffer << "    <link rel=\"stylesheet\" type=\"text/css\" href=\"static/style.css\">" << std::endl;
	outbuffer << "  </head>" << std::endl;
	outbuffer << "  <body>" << std::endl;
	outbuffer << "    <table><tr><td>" << std::endl;
	outbuffer << "      <table width=\"400\" class=\"gridinfo\">" << std::endl;
	outbuffer << "        <tr class=\"gridinfo\"><th colspan=\"2\"  class=\"gridinfo\">Grid information</th></tr>" << std::endl;
	outbuffer << "        <tr  class=\"gridinfo\"><td class=\"gridinfo\">Name:</td><td class=\"gridinfo\">"
			<< servermanager->getServerName() << "</td></tr>" << std::endl;
	outbuffer << "        <tr  class=\"gridinfo\"><td class=\"gridinfo\">Atlasid:</td><td class=\"gridinfo\">"
			<< servermanager->getAtlasId() << "</td></tr>" << std::endl;
	outbuffer << "        <tr  class=\"gridinfo\"><td class=\"gridinfo\">Size:</td><td class=\"gridinfo\">" << servermanager->getMaxX()
			<< "x" << servermanager->getMaxY() << " Grids</td></tr>"
			<< std::endl;
	outbuffer << "      </table>" << std::endl;
	outbuffer << "      </td><td>" << std::endl;

	// grid comes here
	outbuffer << "      <table class=\"gridoverview\">" << std::endl;
	outbuffer << "        <tr class=\"gridoverview\"><th class=\"gridoverview\" colspan=\"" << (servermanager->getMaxX() +1) << "\">Gridoverview:</th></tr>" << std::endl;
	outbuffer << "        <tr class=\"gridoverview\"><th class=\"gridoverview\"></th>";

	for (unsigned int x = 0; x < servermanager->getMaxX(); x++)
	{
		char LexicalXCoord = (0x41 + x);
		outbuffer << "<th class=\"gridoverview\">" << LexicalXCoord << "</th>";
	}
	outbuffer << "</tr>" << std::endl;

	for (unsigned y = 1; y <= servermanager->getMaxY(); y++)
	{
		outbuffer << "        <tr class=\"gridoverview\">";
		for (unsigned int x = 0; x <= servermanager->getMaxX(); x++)
		{
			if (x == 0)
			{
				outbuffer << "<th class=\"gridoverview\">" << y << "</th>";
				continue;
			}

			ServerType type = servermanager->getType(x-1, y-1);
			runReason reason = servermanager->isSupposedToRun(x-1, y-1);

			if (type == Undefined)
			{
				outbuffer << "<td class=\"gridoverview\" bgcolor=red></td>";
				continue;
			}

			if (type == DummyListener)
			{
				if (reason == noRun)
				{
					outbuffer << "<td class=\"gridoverview\" bgcolor=CornFlowerBlue></td>";
				}
				else
				{
					outbuffer << "<td class=\"gridoverview\" bgcolor=yellow></td>";
				}
				continue;
			}

			if (type == AtlasServer)
			{
				if (reason == runHomeServer)
				{
					outbuffer << "<td class=\"gridoverview\" bgcolor=ForestGreen></td>";
				}
				else if (reason == runMissingRedisId)
				{
					outbuffer << "<td class=\"gridoverview\" bgcolor=oliveDrab></td>";
				}
				else {
					outbuffer << "<td class=\"gridoverview\" bgcolor=green></td>";
				}
				continue;
			}

			outbuffer << "<td class=\"gridoverview\" bgcolor=purple></td>";
		}
		outbuffer << "</tr>" << std::endl;
	}
	outbuffer << "        </table>" << std::endl;

	outbuffer << "      </td></tr>" << std::endl;
	outbuffer << "    </table>" << std::endl;
	outbuffer << "    <br><br>" << std::endl;
	outbuffer << "    <table class=\"gridstatus\">" << std::endl;
	outbuffer
			<< "      <tr class=\"gridstatus\">"
			<< "<th class=\"gridstatus\"></th>"
			<< "<th class=\"gridstatus\">Type</th>"
			<< "<th class=\"gridstatus\"></th>"
			<< "<th class=\"gridstatus\">cluster last<br> seen alive</th>"
			<< "<th class=\"gridstatus\">players<br>online / total</th>"
			<< "<th class=\"gridstatus\">time since last<br> player activity</th>"
			<< "<th class=\"gridstatus\">trig.</th>"
			<< "<th class=\"gridstatus\">cpu</th>"
			<< "<th class=\"gridstatus\">mem</th>"
			<< "<th class=\"gridstatus\">rcon</th>"
			<< "<th class=\"gridstatus\">requested state</th></tr>"
			<< std::endl;
	for (unsigned int x = 0; x < servermanager->getMaxX(); x++)
		for (unsigned int y = 0; y < servermanager->getMaxY(); y++) {
			char LexicalXCoord = (0x41 + x);
			outbuffer << "      <tr class=\"gridstatus\"><td class=\"gridstatus\">"
					"<a href=webmap_" << LexicalXCoord << "_" << (y+1) << ">";
			outbuffer << LexicalXCoord << (y + 1) << "</a></td>";
			switch (servermanager->getType(x, y)) {
			case DummyListener:
				outbuffer << "<td class=\"gridstatus\"><img class=\"statusicon\" src=\"static/spacer_icon.png\"></td>";
				break;
			case AtlasServer:
				outbuffer << "<td class=\"gridstatus\"><img class=\"statusicon\" src=\"static/atlas_icon.png\"></td>";
				break;
			case Undefined:
			default:
				outbuffer << "<td class=\"gridstatus\">unknown</td>";
			}
			if (servermanager->isHomeServer(x, y)) {
				outbuffer << "<td class=\"gridstatus\"><img class=\"statusicon\" src=\"static/home_icon.png\"></td>";
			} else {
				outbuffer << "<td class=\"gridstatus\"></td>";
			}
			outbuffer << "<td class=\"gridstatus\">" << servermanager->timeSinceLastOnline(x, y)
					<< " s</td>";
			outbuffer << "<td class=\"gridstatus\">" << servermanager->getActiveUsers(x, y) << "/"
					<< servermanager->getPlayerCount(x, y) << "</td>";
			outbuffer << "<td class=\"gridstatus\">"
					<< servermanager->timeSinceLastPlayerOnline(x, y)
					<< " s</td>";
			std::string istriggering = "-";
			if (servermanager->getType(x, y) == DummyListener) {
				if (servermanager->getFreeServerSlots() > 0)
					istriggering = "yes";
				else
					istriggering = "no";
			}

			std::stringstream cpuusage;
			std::stringstream memusage;
			cpuusage.precision(2);
			memusage.precision(2);
			if (servermanager->getType(x, y) == AtlasServer) {

				cpuusage << std::fixed << (servermanager->getCpuUsage(x,y)*100) << " %";
				memusage << std::fixed << (servermanager->getMemoryUsage(x,y) /1024.0/1024.0/1024.0) << " GB";
			}
			else {
				cpuusage << "-";
				memusage << "0";
			}

			outbuffer << "<td class=\"gridstatus\">" << istriggering << "</td>";
			outbuffer << "<td class=\"gridstatus\">" << cpuusage.str() << "</td>";
			outbuffer << "<td class=\"gridstatus\">" << memusage.str() << "</td>";
			outbuffer << "<td class=\"gridstatus\">" << servermanager->getRconPort(x,y) << "</td>";
			outbuffer << "<td class=\"gridstatus\">"
					<< reasonLookupTable[servermanager->isSupposedToRun(x, y)]
					<< "</td>";
			outbuffer << "</tr>" << std::endl;
		}
	outbuffer << "    </table>" << std::endl;
	outbuffer << "  </body>" << std::endl;
	outbuffer << "</html>" << std::endl;
}

/*****************************************************************************/
std::string WebServer::getImageFromType(std::string type)
{
	if (m_markerTypeImageLookupTable.find(type) == m_markerTypeImageLookupTable.end())
	{
		return "";
	}

	return m_markerTypeImageLookupTable[type];
}

/*****************************************************************************/
void WebServer::readMarkerFile(Json::Value* toset, int x, int y)
{
	std::stringstream markerfilename;
	markerfilename << "dynamic/markers_" << x << "_" << y << ".json";

	std::string markerfile("");

	// read old marker list
	FILE* MARKERFILE = fopen(markerfilename.str().c_str(), "r");

	bool readmarkerfile = false;

	if (MARKERFILE == 0)
	{
		DBG(std::cout << "failed to read from marker file: " << markerfilename.str() << std::endl;)
	}
	else {
		char readbuffer[1024];
		memset(readbuffer,0, sizeof(readbuffer));
		int readlen = 0;
		while((readlen = fread(readbuffer, 1, sizeof(readbuffer), MARKERFILE)) > 0)
		{
			markerfile.append(std::string(readbuffer, readlen));
			memset(readbuffer, 0, sizeof(readbuffer));
		}

		fclose(MARKERFILE);
		readmarkerfile = true;
	}

	Json::Reader reader;
	if ((markerfile.size() > 0) && (!reader.parse(markerfile, *toset)))
	{
		DBG(std::cout  << "Failed to parse markerfile" << std::endl
				   << reader.getFormattedErrorMessages() << std::endl;)
		return;
	}

	if (!readmarkerfile)
	{
		(*toset)["xCoord"] = x;
		(*toset)["yCoord"] = y;
		(*toset)["markers"] = Json::Value(Json::arrayValue);
	}
}

/*****************************************************************************/
void WebServer::saveMarkerFile(Json::Value* toset, int x, int y)
{
	std::stringstream markerfilename;
		markerfilename << "dynamic/markers_" << x << "_" << y << ".json";

	// save markerlist
	Json::FastWriter fastWriter;
	std::string output = fastWriter.write(*toset);

	FILE* MARKERFILE = fopen(markerfilename.str().c_str(), "w");

	if (!MARKERFILE)
	{
		DBG(std::cout  << "Failed to open markerfile for writing" << std::endl;)
		return;
	}

	fwrite(output.c_str(), 1, output.size(), MARKERFILE);
	fclose (MARKERFILE);
}

/*****************************************************************************/
void WebServer::pageHandlerForm(const char* url, size_t postsize, const char* postbuffer)
{

	if (postsize == 0)
	{
		return;
	}

	std::string params(postbuffer,postsize);

	// fix the input string
	findAndReplace(params, "\\\"", "\"");
	findAndReplace(params, "\"{", "{");
	findAndReplace(params, "}\"", "}");

	Json::Value requestroot;
	Json::Reader reader;
	if (!reader.parse(params, requestroot))
	{
		DBG(std::cout << "pageHandlerForm: failed to parse json form data" << std::endl;)
		return;
	}

	if (requestroot.empty())
	{
		DBG(std::cout << "pageHandlerForm: didn't get valid json" << std::endl;)
		return;
	}

	/* command to be executed */
	std::string cmd = requestroot.get("cmd", "nocmd").asString();

	/* parameters required to be present in all commands */
	int x = requestroot.get("xCoord", "-1").asInt();
	int y = requestroot.get("yCoord", "-1").asInt();
	std::string uid = requestroot.get("uid", "").asString();

	if (cmd == "AddMarker")
	{
		double xMarkerPos = requestroot.get("xMarkerPos", "-1").asDouble();
		double yMarkerPos = requestroot.get("yMarkerPos", "-1").asDouble();

		std::string markertype = requestroot.get("markertype", "none").asString();
		std::string markertext = requestroot.get("markertext", "").asString();

		if ((markertype == "none") || (x == -1) || (y == -1) || (xMarkerPos < 0) || (yMarkerPos < 0) || (uid == ""))
		{
			DBG(std::cout << "AddMarker parameter error" << std::endl;)
			return;
		}

		Json::Value root;
		readMarkerFile(&root, x, y);

		// append new marker
		Json::Value new_entry;

		new_entry["uid"] = uid;
		new_entry["xpos"] = xMarkerPos;
		new_entry["ypos"] = yMarkerPos;
		new_entry["image"] = getImageFromType(markertype);


		if (markertext.size() > 0)
			new_entry["text"] = markertype + " (" + markertext + ")";
		else
			new_entry["text"] = markertype;


		root["markers"].append(new_entry);

		saveMarkerFile(&root, x, y);
	}

	if (cmd == "DelMarker")
	{
		std::string uid = requestroot.get("uid", "").asString();

		if ((x == -1) || (y == -1) || (uid == ""))
		{
			DBG(std::cout << "DelMarker parameter error" << std::endl;)
			return;
		}

		Json::Value root;
		readMarkerFile(&root, x, y);
		Json::Value markers = root.get("markers", 0);

		/* jsoncpp doesn't support erasing elements from arrays */
		/* so we need to build a new one */
		Json::Value newroot;
		newroot["xCoord"] = x;
		newroot["yCoord"] = y;
		Json::Value newmarkers = Json::Value(Json::arrayValue);

		for (unsigned int i = 0; i < markers.size(); i++)
		{
			std::string cur_uid = markers[i].get("uid", "").asString();

			if(cur_uid != uid)
			{
				newmarkers.append(markers[i]);
			}
		}
		newroot["markers"] = newmarkers;

		saveMarkerFile(&newroot, x, y);
	}
}

/*****************************************************************************/
int WebServer::answerToConnection(void *cls, struct MHD_Connection *connection,
                  const char *url,
                  const char *method, const char *version,
                  const char *upload_data,
                  size_t *upload_data_size, void **con_cls)
{
	manager* servermanager = reinterpret_cast<manager*>(cls);

	printf("URL: >>%s<< method: %s upload_size: %ld\n", url, method, *upload_data_size);
	std::stringstream outbuffer;

	if (strncmp(method, MHD_HTTP_METHOD_GET, strlen(MHD_HTTP_METHOD_GET)) == 0)
	{
		if ((url[0] == '/') && (url[1] == 0))
		{
			pageHandlerStatusOld(outbuffer, servermanager);
		}
		/* seems a request for a map image */
		else if ((strncmp(url, "/CellImg_", strlen("/CellImg_")) == 0) ||
				(strncmp(url, "/MapImg", strlen("/MapImg")) == 0) )
		{
			pageHandlerServerGridImages(url, outbuffer, servermanager);
		}
		else if (strncmp(url, "/static/", strlen("/static/")) == 0)
		{
			pageHandlerStaticFiles(url, outbuffer);
		}
		else if (strncmp(url, "/webmap_", strlen("/webmap_")) == 0)
		{
			pageHandlerWebMap(url, outbuffer);
		}
		else  if (strncmp(url, "/dynamic/", strlen("/dynamic/")) == 0)
		{
			pageHandlerDynamicFiles(url, outbuffer, servermanager);
		}

		struct MHD_Response *response;
		int ret = 0;
		outbuffer.seekp(0, std::ios::end);
		std::stringstream::pos_type offset = outbuffer.tellp();

		if (offset > 0)
		{
			response = MHD_create_response_from_buffer (outbuffer.str().size(),
														(void*) outbuffer.str().c_str(),
														MHD_RESPMEM_MUST_COPY);
			ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
		}
		else {
			response = MHD_create_response_from_buffer (strlen(file_not_found_404),
														(void*) file_not_found_404,
			                                            MHD_RESPMEM_PERSISTENT);
			ret = MHD_queue_response(connection, MHD_HTTP_NOT_FOUND, response);
		}
		MHD_destroy_response (response);
		return ret;
	}

	if (strncmp (method, MHD_HTTP_METHOD_POST, strlen(MHD_HTTP_METHOD_POST)) == 0)
	{
		bool * initial_call = (bool *) *con_cls;

		/* initial call */
		if (initial_call == 0)
		{
			initial_call = (bool*) malloc(sizeof(bool));

			if (initial_call == 0)
			{
				return MHD_NO;
			}

			*initial_call = false;
			*con_cls = (void*) initial_call;
			return MHD_YES;
		}

		if (*upload_data_size)
		{
			if (strncmp(url, "/formhandler", strlen("/formhandler")) == 0)
			{
				try {
					pageHandlerForm(url, *upload_data_size, upload_data);
				}
				catch(const Json::LogicError& le )
				{
					return MHD_NO;
				}
			}
			/* if we don't know the page return false */
			else {
				return MHD_NO;
			}
			*upload_data_size = 0;
			return MHD_YES;
		}
		else {
			struct MHD_Response *response;
			response = MHD_create_response_from_buffer (strlen(file_ok_200),
														(void*) file_ok_200,
														MHD_RESPMEM_PERSISTENT);

			// cleanup the post marker
			free(initial_call);

			int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
			MHD_destroy_response (response);
			return ret;
		}
	}

	return MHD_NO;
}

#endif /* HAVE_INTERNAL_WEBSERVER */
