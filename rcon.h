/*
 * rcon.h
 *
 *  Created on: 29.01.2019
 *      Author: sapier
 */

#ifndef RCON_H_
#define RCON_H_

bool runRconCMD(const char* ip, uint16_t port, const char* passwd, const char* cmd);


#endif /* RCON_H_ */
