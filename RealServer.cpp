/*
 * RealServer.cpp
 *
 *  Created on: 27.01.2019
 *      Author: sapier
 */

#include "switches.h"

#include <ctime>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <string.h>

#include "RealServer.hpp"
#include "iserver.hpp"
#include "rcon.h"

RealServer::RealServer(redisContext* context, std::string basepath, int x, int y ) :
	IServer(AtlasServer),
	m_context(context),
	m_rediscluster(),
	m_basepath(basepath),
	m_queryPort(0),
	m_mapname("Ocean"),
#ifdef USE_WINE
	m_binarypath("ShooterGame/Binaries/Win64"),
	m_appname("/usr/bin/wine64"),
#else /* USE_WINE */
	m_binarypath("ShooterGame/Binaries/Linux"),
	m_appname("./ShooterGameServer"),
#endif /* USE_WINE */
	m_serverpid(-1),
	m_tsLastPlayerActive(0),
	m_rconport(0),
	m_rconpwd("keins"),
	m_totalusage(0),
	m_procusage(0),
	m_memoryusage(0),
	m_cpuusage(0)
{
	m_valid = true;
	m_gridX = x;
	m_gridY = y;
}

RealServer::~RealServer()
{
	if (m_serverpid > 0)
	{
		if (!runRconCMD("127.0.0.1", m_rconport, m_rconpwd.c_str(), "saveworld"))
		{
			return;
		}

		if (!runRconCMD("127.0.0.1", m_rconport, m_rconpwd.c_str(), "quit"))
		{
			return;
		}

		int child_retval;
		waitpid(m_serverpid, &child_retval, 0);
	}
}

void RealServer::runServer()
{

	if (m_serverpid > 0) {
		std::cout << "Server is already running" << std::endl;
		return;
	}
	/* prepare command for running as different user */
	std::string cmdline("");
	cmdline += buildCmdLineArgs();

	std::cout << "Server cmdline:\n" << m_appname << " " << cmdline << std::endl;

	m_serverpid = fork();

	if (m_serverpid < 0) {
		return;
	}

	if (m_serverpid == 0)
	{
		std::string path = m_basepath + "/" + m_binarypath;
		chdir(path.c_str());

#ifdef USE_WINE
		execl(m_appname.c_str(), "wine64", "ShooterGameServer.exe", cmdline.c_str(),
				"-log", "-server", "-NoBattlEye", "-insecure", 0);
#else /* USE_WINE */
		execl(m_appname.c_str(), "ShooterGameServer", cmdline.c_str(),
				"-log", "-server", "-NoBattlEye", "-insecure", 0);
#endif /* USE_WINE */
		perror("execl:");

		exit(-1);
	}
}

int RealServer::getActiveUsers()
{
	if (!m_valid)
	{
		std::cout << "RealServer::getActiveUsers() for invalid server" << std::endl;
	}

	if (m_rediscluster.size() == 0)
	{
		m_rediscluster = getClusterByPort(m_context, m_queryPort);
	}

	if (m_rediscluster.size() == 0)
	{
		std::cout << "RealServer::getActiveUsers() no redisclusterid present!" << std::endl;
		return 0;
	}


	int freeReservedSlots = 0;

	redisReply* creply = (redisReply*) redisCommand(m_context, "HGET %s NumFreeReservedSlots", m_rediscluster.c_str());
	if (creply) {
		freeReservedSlots = std::stod(creply->str);
		freeReplyObject(creply);
		creply = 0;
	}
	else {
		std::cout << "RealServer::getActiveUsers() failed to get NumFreeReservedSlots" << std::endl;
	}

	if (m_parameters.find("MaxPlayers") != m_parameters.end())
	{
		int retval = std::stod(m_parameters["MaxPlayers"]) - freeReservedSlots;

		if (retval > 0) {
			m_tsLastPlayerActive = getTimeStamp();
		}
		return retval;
	}
	else {
		std::cout << "RealServer::getActiveUsers() missing slot config" << std::endl;
	}
	m_tsLastPlayerActive = getTimeStamp();

	/* well we don't really know so tell some crazy high number */
	return 99999;
}

std::string RealServer::buildCmdLineArgs()
{
	std::string retval;

	retval += m_mapname;

	retval +="?ServerX=" + std::to_string(m_gridX);
	retval +="?ServerY=" + std::to_string(m_gridY);

	for (auto iter = m_parameters.begin(); iter != m_parameters.end(); iter++)
	{
		retval += "?" + iter->first + "=" + iter->second;
	}

	retval += "?RCONEnabled=True";
	retval += "?RCONPort=" + std::to_string(m_rconport);
	retval += "?ServerAdminPassword=" + m_rconpwd;

	return retval;
}

int RealServer::timeSinceLastKnownAlive()
{

	int lastUpdated = 0;
	int now = getTimeStamp();

	redisReply* creply = (redisReply*) redisCommand(m_context, "HGET %s LastUpdated", m_rediscluster.c_str());
	if ((creply) && (creply->str)) {
		lastUpdated = std::stod(creply->str);
	}
	else {
		std::cout << "RealServer::tsLastAlive() failed to get LastUpdated" << std::endl;
	}

	if (creply)
		freeReplyObject(creply);

	return now - lastUpdated;
}

unsigned int RealServer::timeSinceLastPlayerActive()
{
	return getTimeStamp() - m_tsLastPlayerActive;
}

int RealServer::getTimeStamp()
{
	std::time_t result = std::time(nullptr);
	return result;
}

void RealServer::setQueryPort(int port)
{
	m_queryPort = port;
	m_rediscluster = getClusterByPort(m_context, port);
	m_parameters["QueryPort"] = std::to_string(port);

	m_rconport = (port % 100) + 8000;
}

void RealServer::updateServerId()
{
	if (m_ServerId.size() != 0)
	{
		return;
	}

	redisReply* creply = (redisReply*) redisCommand(m_context, "HGET %s ServerId", m_rediscluster.c_str());
	if ((creply) && (creply->str)) {
		m_ServerId = creply->str;
		freeReplyObject(creply);
		creply = 0;
	}
}

int RealServer::getRconPort()
{
	return m_rconport;
}

void RealServer::updateSysStats()
{
	FILE* totalcpuusage = fopen("/proc/stat", "r");

	if (!totalcpuusage)
	{
		printf("RealServer::updateSysStats failed to open /proc/stat\n");
		return;
	}

	char buffer[1024];

	long long unsigned int user, nice, system, idle = 0;
	fread(buffer, 1, sizeof(buffer), totalcpuusage);
	fclose(totalcpuusage);

	sscanf(buffer, "cpu %llu %llu %llu %llu", &user, &nice, &system, &idle);

	char filename[512];
	snprintf(filename, sizeof(filename), "/proc/%d/stat", m_serverpid);

	FILE* cpuusage = fopen(filename, "r");

	if (!cpuusage)
	{
		printf("RealServer::updateSysStats failed to open %s\n", filename);
		return;
	}

	fread(buffer, 1, sizeof(buffer), cpuusage);
	fclose(cpuusage);

	long unsigned int usertime, systemtime = 0;
	sscanf(buffer,
			"%*d %*s %*c %*d" //pid,command,state,ppid
			"%*d %*d %*d %*d %*u %*lu %*lu %*lu %*lu"
			"%lu %lu" //usertime,systemtime
			"%*ld %*ld %*ld %*ld %*ld %*ld %*llu"
			"%*lu %lu", //virtual memory size in bytes, resident set size in pages
			&usertime, &systemtime, &m_memoryusage);

	long unsigned int totaltime = user + nice + system + idle;
	long unsigned int proctime = usertime + systemtime;

	int nb = sysconf(_SC_NPROCESSORS_ONLN);
	int pagesize = sysconf(_SC_PAGE_SIZE);

	m_cpuusage = (proctime - m_procusage) * nb / (float) (totaltime - m_totalusage);
	m_memoryusage *= pagesize;

	m_totalusage = totaltime;
	m_procusage = proctime;
}
