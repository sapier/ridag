/*
 * manager.cpp
 *
 *  Created on: 29.01.2019
 *      Author: sapier
 */
#include "switches.h"

#include <iostream>
#include <stdio.h>
#include <ctime>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <libgen.h>

#include "manager.h"
#include "common.hpp"
#include "ServerDummy.hpp"
#include "RealServer.hpp"

const char* reasonLookupTable_[] =
{
		"noRun",
		"runHomeServer",
		"runPlayerCount",
		"runMissingRedisId",
		"runActivePlayers",
		"runNoActiveCountYet",
		"runNotYetTimedOut"
};

const char** reasonLookupTable = reasonLookupTable_;

/*****************************************************************************/
manager::manager(const char* servergridfile, const char* redisip,
		uint16_t redisport, const char* redispwd, bool isDummy) :
	m_valid(false),
	m_xGrids(0),
	m_yGrids(0),
	m_servername(""),
	m_maxActiveServers(0),
	m_inactivityTimeout(9999999),
	m_applicationbase(""),
	m_lastTriggered(std::time(nullptr)),
	m_serverdir(""),
	m_maxplayers(30),
	m_reservedplayers(10),
	m_pveEnabled(true),
	m_isDummy(isDummy)
{
	char buffer[PATH_MAX];
	memset(buffer, 0, sizeof(buffer));
	strncpy(buffer, servergridfile, sizeof(buffer));
	std::string servergriddir = dirname(buffer);
	servergriddir += "/../";
	memset(buffer, 0, sizeof(buffer));
	realpath(servergriddir.c_str(),buffer);
	m_serverdir = buffer;

	FILE* SERVERGRID = fopen(servergridfile, "r");

	if (SERVERGRID == 0)
	{
		DBG(std::cout << "manager::manager failed to open servergridfile" << servergridfile << std::endl;)
		return;
	}

	std::string gridfile("");
	char readbuffer[1024];
	memset(readbuffer,0, sizeof(readbuffer));
	int readlen = 0;
	while((readlen = fread(readbuffer, 1, sizeof(readbuffer), SERVERGRID)) > 0)
	{
		gridfile.append(std::string(readbuffer, readlen));
		memset(readbuffer, 0, sizeof(readbuffer));
	}

	fclose(SERVERGRID);

	Json::Value root;
	Json::Reader reader;
	if (!reader.parse(gridfile, root))
	{
		DBG(std::cout  << "Failed to parse configuration" << std::endl
	               << reader.getFormattedErrorMessages() << std::endl;)
		return;
	}

	m_xGrids = root.get("totalGridsX", "0" ).asInt();
	m_yGrids = root.get("totalGridsY", "0" ).asInt();
	m_servername = root.get("WorldFriendlyName", "I have no name").asString();
	m_WorldAtlasId = root.get("WorldAtlasId", "0").asString();

	readServerList(root);


	m_rediscontext = redisConnect(redisip, redisport);

	if ((m_rediscontext == 0) || (m_rediscontext->err))
	{
		if (m_rediscontext) {
			DBG(std::cout << "manager::manager redis connection error:" << m_rediscontext->errstr << std::endl);
		}
		else {
			DBG(std::cout << "manager::manager redis connection failed" << std::endl);
		}
		return;
	}

	redisReply *reply = (redisReply*) redisCommand(m_rediscontext, "AUTH %s", redispwd);

	if ((reply == 0) || (std::string(reply->str) != std::string("OK")))
	{
		freeReplyObject(reply);
		return;
	}
	freeReplyObject(reply);

	updateredisIds();

	m_valid = true;
}

/*****************************************************************************/
manager::~manager()
{
	for (unsigned int x = 0; x < m_xGrids; x++)
	{
		for (unsigned int y = 0; y < m_yGrids; y++)
		{
			if (m_servers[x][y].m_server != 0)
			{
				delete m_servers[x][y].m_server;
			}
		}
	}
}

/*****************************************************************************/
void manager::readServerList(const Json::Value& root)
{
	Json::Value json_serverlist = root.get("servers", 0);

	for (unsigned int i = 0; i < json_serverlist.size(); i++)
	{
		Json::Value element = json_serverlist.get(i, 0);
		int x = element.get("gridX", -1).asInt();
		int y = element.get("gridY", -1).asInt();
		if ((x < 0) || (y < 0)) {
			printf("Failed to get grid info for server!\n");
			exit(-1);
		}
		m_servers[x][y].m_isHomeserver = element.get("isHomeServer", false).asBool();
		m_servers[x][y].m_Ip 		   = element.get("ip", "0.0.0.0").asString();
		m_servers[x][y].m_port         = element.get("port", 0).asInt();
		m_servers[x][y].m_server       = 0;
	}
}

/*****************************************************************************/
void manager::triggerDummys()
{
	if ((std::time(nullptr) - m_lastTriggered) > 10)
	{
		for (auto x_iter = m_servers.begin(); x_iter != m_servers.end(); x_iter++)
		{
			for ( auto y_iter = x_iter->second.begin(); y_iter != x_iter->second.end(); y_iter++)
			{
				ServerDummy* totrigger = dynamic_cast<ServerDummy*>(y_iter->second.m_server);
				if ((totrigger != 0) && (totrigger->isValid()))
				{
					totrigger->publishServer(m_maxActiveServers > 0 ? 1 : 0);
				}
			}
		}

		m_lastTriggered = std::time(nullptr);
	}
}

/*****************************************************************************/
runReason manager::isSupposedToRun(int x, int y)
{
	if (m_servers.find(x) != m_servers.end())
	{
		if (m_servers[x].find(y) != m_servers[x].end())
		{
			RealServer* tocheck = dynamic_cast<RealServer*>(m_servers[x][y].m_server);

			/* we will always read the active players (if possible)
			 * as this will update the internal timers */
			int activeplayers = 0;
			if (tocheck)
			{
				activeplayers = tocheck->getActiveUsers();
			}

			/* if it's a homeserver and we didn't exceed the active server */
			/* limit start it immediately */
			// TODO add config option to only start one home server in case of multiple ones
			if (m_servers[x][y].m_isHomeserver)
			{
				return runHomeServer;
			}

			if ((m_servers[x][y].m_redisId.size() > 0) &&
					(readPlayerCount(m_rediscontext, m_servers[x][y].m_redisId) > 0))
			{
				return runPlayerCount;
			}

			if (m_servers[x][y].m_redisId.size() == 0)
			{
				return runMissingRedisId;
			}

			if (tocheck == 0)
			{
				return noRun;
			}

			/* in case of active players don't shut down */
			if (activeplayers > 0)
				return runActivePlayers;


			if (tocheck->timeSinceLastKnownAlive() < 0)
				return runNoActiveCountYet;

			/* finaly check how long no one has been on this server */
			if (tocheck->timeSinceLastPlayerActive() < m_inactivityTimeout)
			{
				return runNotYetTimedOut;
			}

		}
	}
	return noRun;
}


/*****************************************************************************/
void manager::checkServerList()
{
	for (unsigned int x = 0; x < m_xGrids; x++)
	{
		for (unsigned int y = 0; y < m_yGrids; y++)
		{
			// try to update redisid in case it's missing
			if ((m_servers[x][y].m_server != 0) &&
					(m_servers[x][y].m_server->getType() == AtlasServer) &&
					(m_servers[x][y].m_redisId.size() == 0))
			{
				m_servers[x][y].m_redisId = getClusterByPort(m_rediscontext, m_servers[x][y].m_port);
			}

			if ((!m_isDummy) &&
				(m_servers[x][y].m_server != 0) &&
				(m_servers[x][y].m_server->getType() == AtlasServer))
			{
				RealServer* server = dynamic_cast<RealServer*>(m_servers[x][y].m_server);

				if (server != 0)
				{
					server->updateSysStats();
				}
			}

			runReason reason = isSupposedToRun(x, y);

			if (( reason != noRun) && (m_maxActiveServers > 0))
			{
				if ((m_servers[x][y].m_server != 0) &&
					(m_servers[x][y].m_server->getType() == DummyListener))
				{
					delete m_servers[x][y].m_server;
					m_servers[x][y].m_server = 0;
				}

				if (m_servers[x][y].m_server == 0)
				{
					// check if
					std::cout << "Server x=" << x << " y=" << y << " starting server because of " << reasonLookupTable[reason] << std::endl;
					m_servers[x][y].m_server = startRealServer(&m_servers[x][y], x, y);

					if (m_servers[x][y].m_server != 0)
					{
						m_maxActiveServers--;
					}
				}
			}
			else if (reason == noRun) {
				if ((m_servers[x][y].m_server != 0) &&
						(m_servers[x][y].m_server->getType() == AtlasServer))
				{
					// TODO shutdown
					std::cout << "Server x=" << x << " y=" << y << " is no longer needed " << std::endl;
					delete m_servers[x][y].m_server;
					m_servers[x][y].m_server = 0;
					m_maxActiveServers++;
				}

				/* if we do have an id, create a dummy */
				if ((m_servers[x][y].m_server == 0) &&
						(m_servers[x][y].m_redisId.size() >= 0))
				{
					m_servers[x][y].m_server = new ServerDummy(m_rediscontext, m_servers[x][y].m_port, x, y);
				}
			}
		}
	}

	triggerDummys();
}

/*****************************************************************************/
bool manager::isHomeServer(unsigned int x, unsigned int y)
{
	if (m_servers.find(x) != m_servers.end())
	{
		if (m_servers[x].find(y) != m_servers[x].end())
		{
			return m_servers[x][y].m_isHomeserver;
		}
	}
	return false;
}

/*****************************************************************************/
ServerType manager::getType(unsigned int x, unsigned int y)
{
	if (m_servers.find(x) != m_servers.end())
	{
		if (m_servers[x].find(y) != m_servers[x].end())
		{
			if (m_servers[x][y].m_server != 0)
				return m_servers[x][y].m_server->getType();

		}
	}
	return Undefined;
}

/*****************************************************************************/
bool manager::isAtlasServer(unsigned int x, unsigned int y)
{
	if (m_servers.find(x) != m_servers.end())
		{
			if (m_servers[x].find(y) != m_servers[x].end())
			{
				if ((m_servers[x][y].m_server != 0) && (m_servers[x][y].m_server->getType() == AtlasServer))
				{
					return true;
				}
			}
		}
	return false;
}

/*****************************************************************************/
int manager::getPlayerCount(unsigned int x, unsigned int y)
{
	if (m_servers.find(x) != m_servers.end())
	{
		if (m_servers[x].find(y) != m_servers[x].end())
		{
			if (m_servers[x][y].m_redisId.size() > 0)
			{
				return readPlayerCount(m_rediscontext, m_servers[x][y].m_redisId);
			}
		}
	}

	return 0;
}

/*****************************************************************************/
int manager::getActiveUsers(unsigned int x, unsigned int y)
{
	if (!isAtlasServer(x,y))
	{
		return 0;
	}

	RealServer* tocheck = dynamic_cast<RealServer*>(m_servers[x][y].m_server);

	if (tocheck != 0)
		return tocheck->getActiveUsers();

	return 0;
}

/*****************************************************************************/
long unsigned int manager::getMemoryUsage(unsigned int x, unsigned int y)
{
	if (!isAtlasServer(x,y))
	{
		return 0;
	}

	RealServer* tocheck = dynamic_cast<RealServer*>(m_servers[x][y].m_server);

	if (tocheck != 0)
		return tocheck->getMemoryUsage();

	return 0;
}

/*****************************************************************************/
float manager::getCpuUsage(unsigned int x, unsigned int y)
{
	if (!isAtlasServer(x,y))
	{
		return 0;
	}

	RealServer* tocheck = dynamic_cast<RealServer*>(m_servers[x][y].m_server);

	if (tocheck != 0)
		return tocheck->getCpuUsage();

	return 0;
}

/*****************************************************************************/
int manager::getRconPort(unsigned int x, unsigned int y)
{
	if (!isAtlasServer(x,y))
	{
		return 0;
	}

	RealServer* tocheck = dynamic_cast<RealServer*>(m_servers[x][y].m_server);

	if (tocheck != 0)
		return tocheck->getRconPort();

	return 0;
}

/*****************************************************************************/
bool manager::isTriggering(unsigned int x, unsigned int y)
{
	if (m_servers.find(x) != m_servers.end())
	{
		if (m_servers[x].find(y) != m_servers[x].end())
		{
			return (m_servers[x][y].m_server != 0) &&
					(m_servers[x][y].m_server->getType() != AtlasServer) &&
					(m_servers[x][y].m_server->isValid());
		}
	}
	return false;
}

/*****************************************************************************/
int manager::timeSinceLastPlayerOnline(unsigned int x, unsigned int y)
{
	if (m_servers.find(x) != m_servers.end())
	{
		if (m_servers[x].find(y) != m_servers[x].end())
		{
			RealServer* tocheck = dynamic_cast<RealServer*>(m_servers[x][y].m_server);

			if (tocheck != 0)
					return tocheck->timeSinceLastPlayerActive();
		}
	}
	return -1;
}

/*****************************************************************************/
int manager::timeSinceLastOnline(unsigned int x, unsigned int y)
{
	if (m_servers.find(x) != m_servers.end())
	{
		if (m_servers[x].find(y) != m_servers[x].end())
		{
			if (m_servers[x][y].m_redisId.size() > 0)
			{
				return std::time(nullptr) - readTimeSinceClusterLastOnline(m_rediscontext, m_WorldAtlasId, m_servers[x][y].m_redisId);
			}
		}
	}
	return -1;
}

/*****************************************************************************/
void manager::frameForPort(int port)
{
	for (auto x_iter = m_servers.begin(); x_iter != m_servers.end(); x_iter++)
	{
		for ( auto y_iter = x_iter->second.begin(); y_iter != x_iter->second.end(); y_iter++)
		{
			if (y_iter->second.m_port != port)
				continue;

			ServerDummy* totrigger = dynamic_cast<ServerDummy*>(y_iter->second.m_server);
			if ((totrigger != 0) && (totrigger->isValid()))
			{
				if (m_maxActiveServers <= 0)
					return;

				delete y_iter->second.m_server;
				y_iter->second.m_server = 0;

				std::cout << "Server x=" << x_iter->first << " y=" << y_iter->first
						<< " started because of incoming frame for port" << std::endl;
				y_iter->second.m_server = startRealServer(&y_iter->second, x_iter->first, y_iter->first);

				if (y_iter->second.m_server != 0)
					m_maxActiveServers--;
			}
		}
	}
}

/*****************************************************************************/
IServer* manager::startRealServer(serverinfo* tostart, int x, int y)
{
	if (tostart == 0)
		return 0;

	if (tostart->m_server != 0)
		return tostart->m_server;


	/* create real server here! */
	class RealServer* toconfigure = new class RealServer(m_rediscontext, m_serverdir, x, y);

	toconfigure->setWANAddress(tostart->m_Ip);
	toconfigure->setMaxPlayers(m_maxplayers);
	toconfigure->setMaxReservedPlayers(m_reservedplayers);
	toconfigure->setStorageDirPrefix("");   // TODO maybe use atlasid
	toconfigure->setServerPVE(m_pveEnabled);
	toconfigure->setQueryPort(tostart->m_port);
	toconfigure->setSeamlessIp(tostart->m_Ip);


	if (!m_isDummy)
	{
		std::cout << "Starting server for grid x=" << x << " y=" << y << std::endl;
		toconfigure->runServer();
	}
	return toconfigure;
}

/*****************************************************************************/
void manager::updateredisIds()
{
	redisReply *reply = (redisReply*) redisCommand(m_rediscontext, "KEYS cluster*");

	for (unsigned int j=0; j < reply->elements; j++)
	{
		redisReply *creply = (redisReply*) redisCommand(m_rediscontext, "HGET %s Port", reply->element[j]->str);

		if (creply)
		{
			int port = std::stoi(creply->str);
			freeReplyObject(creply);

			for (auto x_iter = m_servers.begin(); x_iter != m_servers.end(); x_iter++)
			{
				for ( auto y_iter = x_iter->second.begin(); y_iter != x_iter->second.end(); y_iter++)
				{
					if (y_iter->second.m_port == port)
					{
						redisReply* idreply = (redisReply*) redisCommand(m_rediscontext, "HGET %s ServerId", reply->element[j]->str);
						if (idreply) {
							y_iter->second.m_redisId = idreply->str;
							freeReplyObject(idreply);
							creply = 0;
						}

						// yeah I know goto is ugly but here it's the best option
						goto end_loop;
					}
				}
			}
			end_loop:;
		}
	}
	freeReplyObject(reply);
}
