#include "ServerDummy.hpp"
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <ctime>

#include "common.hpp"

ServerDummy::ServerDummy(redisContext* context, int cluster_port, int x, int y) :
		IServer(DummyListener),
		m_context(context),
		m_tsLastPublished(0),
		m_statePublished(0)
{
	m_gridX = x;
	m_gridY = y;
	m_Port = std::to_string(cluster_port);
	m_clusterid = getClusterByPort(context, cluster_port);


	if (m_clusterid.size() == 0)
	{
		return;
	}

	redisReply* creply = (redisReply*) redisCommand(m_context, "HGET %s AtlasId", m_clusterid.c_str());

	if (creply) {
		m_AtlasId = creply->str;
		freeReplyObject(creply);
		creply = 0;
	}

	creply = (redisReply*) redisCommand(m_context, "HGET %s ServerId", m_clusterid.c_str());
	if (creply) {
		m_ServerId = creply->str;
		freeReplyObject(creply);
		creply = 0;
	}

	creply = (redisReply*) redisCommand(m_context, "HGET %s Ip", m_clusterid.c_str());
	if (creply) {
		m_Ip = creply->str;
		freeReplyObject(creply);
		creply = 0;
	}

	creply = (redisReply*) redisCommand(m_context, "HGET %s GameIp", m_clusterid.c_str());
	if (creply) {
		m_GameIp = creply->str;
		freeReplyObject(creply);
		creply = 0;
	}

	creply = (redisReply*) redisCommand(m_context, "HGET %s GamePort", m_clusterid.c_str());
	if (creply) {
		m_GamePort = creply->str;
		freeReplyObject(creply);
		creply = 0;
	}

	m_valid = true;
}



ServerDummy::~ServerDummy()
{

}

bool ServerDummy::publishServer(int available_servers)
{
	if (!m_valid)  {
		return false;
	}

#if 0
	if (((std::time(nullptr) - m_tsLastPublished) < 100) &&
			(m_statePublished == available_servers))
	{
		return true;
	}
#endif

	m_tsLastPublished = std::time(nullptr);
	m_statePublished = available_servers;

	bool retval = true;

	char msgbuffer[256];
	memset(msgbuffer, 0, sizeof(msgbuffer));
	snprintf(msgbuffer, sizeof(msgbuffer),"{"
			"\"atlasId\": \"%s\","
			" \"serverId\": %s,"
			" \"ip\": \"%s\","
			" \"port\": %s,"
			" \"gameIp\": \"%s\","
		    " \"gamePort\": %s,"
			" \"numFreeRegularSlots\": %d,"
			" \"numFreeReservedSlots\": %d,"
			" \"lastUpdated\": %d,"
			" \"lastLogLine\": 0"
			" }",
			m_AtlasId.c_str(), m_ServerId.c_str(),
			m_Ip.c_str(), m_Port.c_str(),
			m_GameIp.c_str(),
			m_GamePort.c_str(),
			available_servers > 0 ? 1 : 0, available_servers > 0 ? 1 : 0,
			std::time(nullptr));
	redisReply *reply = (redisReply*) redisCommand(m_context,
			"PUBLISH cluster:channel %s", msgbuffer);

	if (!reply)
	{
		retval = false;
	}
	else {
		freeReplyObject(reply);
	}

	reply = (redisReply*) redisCommand(m_context,
				"HSET %s %s", m_clusterid.c_str(), std::to_string(std::time(nullptr)).c_str());

	if (!reply)
	{
		retval = false;
	}
	else {
		freeReplyObject(reply);
	}

	return retval;
}
