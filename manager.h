/*
 * manager.h
 *
 *  Created on: 29.01.2019
 *      Author: sapier
 */

#ifndef MANAGER_H_
#define MANAGER_H_

#include <jsoncpp/json/json.h>
#include <hiredis/hiredis.h>
#include <set>
#include <ctime>

#include "iserver.hpp"

struct serverinfo
{
	bool m_isHomeserver;
	int  m_port;
	std::string m_Ip;
	std::string m_redisId;
	IServer* m_server;
};

typedef enum {
	noRun = 0,
	runHomeServer,
	runPlayerCount,
	runMissingRedisId,
	runActivePlayers,
	runNoActiveCountYet,
	runNotYetTimedOut
} runReason;

extern const char** reasonLookupTable;

class manager
{
	public:

		manager(const char* servergridfile, const char* redisip, uint16_t redisport, const char* redispwd, bool isDummy);

		~manager();

		void setMaxActiveServers(unsigned int maxactiveservers)
		{ m_maxActiveServers = maxactiveservers; }

		void setInactivityTimeout(unsigned int inactivityTime)
		{ m_inactivityTimeout = inactivityTime; }

		void setMaxPlayers(unsigned int maxplayers)
		{ m_maxplayers = maxplayers; }

		void setPVE(bool enabled)
		{ m_pveEnabled = enabled; }


		unsigned int getMaxX()
		{ return m_xGrids; }

		unsigned int getMaxY()
		{ return m_yGrids; }

		std::string getServerName()
		{ return m_servername; }

		std::string getServerDir()
		{ return m_serverdir; }

		std::string getAtlasId()
		{ return m_WorldAtlasId; }

		bool isHomeServer(unsigned int x, unsigned int y);

		ServerType getType(unsigned int x, unsigned int y);
		int getPlayerCount(unsigned int x, unsigned int y);
		int getActiveUsers(unsigned int x, unsigned int y);
		bool isTriggering(unsigned int x, unsigned int y);
		int timeSinceLastPlayerOnline(unsigned int x, unsigned int y);
		int timeSinceLastOnline(unsigned int x, unsigned int y);
		long unsigned int getMemoryUsage(unsigned int x, unsigned int y);
		float getCpuUsage(unsigned int x, unsigned int y);
		int getRconPort(unsigned int x, unsigned int y);

		bool isValid()
		{ return m_valid; }


		std::set<uint16_t>& getDummyPorts();

		/**
		 * check serverlist for actions to be done
		 * e.g. start homeserver, stop inactive server, create dummy, trigger dummys
		 */
		void checkServerList();


		/**
		 * a frame for a certain port was received
		 */
		void frameForPort(int portnr);

		int getFreeServerSlots()
		{
			return m_maxActiveServers;
		}

		runReason isSupposedToRun(int x, int y);

	private:
		bool m_valid;
		unsigned int m_xGrids;
		unsigned int m_yGrids;
		std::string m_servername;
		std::string m_WorldAtlasId;

		void readServerList(const Json::Value& root);

		IServer* startRealServer(serverinfo* info, int x, int y);

		void updateredisIds();
		void triggerDummys();

		bool isAtlasServer(unsigned int x, unsigned int y);

		unsigned int m_maxActiveServers;
		unsigned int m_inactivityTimeout;

		std::string m_applicationbase;
		redisContext *m_rediscontext;
		std::map<int,std::map<int,struct serverinfo>> m_servers;

		std::time_t m_lastTriggered;
		std::string m_serverdir;

		unsigned int m_maxplayers;
		unsigned int m_reservedplayers;
		bool         m_pveEnabled;

		bool         m_isDummy;
};



#endif /* MANAGER_H_ */
