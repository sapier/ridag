/*
 * rcon.cpp
 *
 *  Created on: 29.01.2019
 *      Author: sapier
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>



// TODO replace lower code
#define SERVERDATA_EXECCOMMAND 2
#define SERVERDATA_AUTH 3
#define SERVERDATA_RESPONSE_VALUE 0
#define SERVERDATA_AUTH_RESPONSE 2

#define DEBUG 0

int send_rcon(int sock, int id, int command, const char *string1, const char *string2) {
  int size, ret;
  size = 10+strlen(string1)+strlen(string2);

  ret = send(sock,&size,sizeof(int),0);
  if(ret == -1) {
    perror("send() failed:");
    return -1;
  }
  ret = send(sock,&id,sizeof(int),0);
  if(ret == -1) {
    perror("send() failed:");
    return -1;
  }
  ret = send(sock,&command,sizeof(int),0);
  if(ret == -1) {
    perror("send() failed:");
    return -1;
  }
  ret = send(sock,string1,strlen(string1)+1,0);
  if(ret == -1) {
    perror("send() failed:");
    return -1;
  }
  ret = send(sock,string2,strlen(string2)+1,0);
  if(ret == -1) {
    perror("send() failed:");
    return -1;
  }
  if(DEBUG) printf("Sent %d bytes\n",size+4);
  return 0;
}

int recv_rcon(int sock, int timeout, int *id, int *command, char *string1,
       char *string2) {
  struct timeval tv;
  fd_set readfds;
  int size;
  char *ptr;
  int ret;
  char buf[8192];

  size=0xDEADBEEF;
  *id=0xDEADBEEF;
  *command=0xDEADBEEF;
  string1[0]=0;
  string2[0]=0;

  tv.tv_sec = timeout;
  tv.tv_usec = 0;

  FD_ZERO(&readfds);
  FD_SET(sock, &readfds);

  /* don't care about writefds and exceptfds: */
  select(sock+1, &readfds, NULL, NULL, &tv);

  if (!FD_ISSET(sock, &readfds)) {
    if(DEBUG) {
      printf("recv timeout\n");
    }
    return -1; // timeout
  }
  if(DEBUG) printf("Got a response\n");
  ret = recv(sock, &size, sizeof(int), 0);
  if(ret == -1) {
    perror("recv() failed:");
    return -1;
  }
  if((size<10) || (size>8192)) {
    printf("Illegal size %d\n",size);
    return -1;
  }
  ret = recv(sock, id, sizeof(int),0);
  if(ret == -1) {
    perror("recv() failed:");
    return -1;
  }
  size-=ret;
  ret = recv(sock, command, sizeof(int),0);
  if(ret == -1) {
    perror("recv() failed:");
    return -1;
  }
  size-=ret;

  ptr = buf;
  while(size) {
    ret = recv(sock, ptr, size, 0);
    if(ret == -1) {
      perror("recv() failed:");
      return -1;
    }
    size -= ret;
    ptr += ret;
  }
  buf[8190] = 0;
  buf[8191] = 0;

  strncpy(string1, buf, 4095);
  string1[4095] = 0;
  strncpy(string2, buf+strlen(string1)+1, 4095);

  return 0;
}

int auth = 0;
char string1[4096];
char string2[4096];

int process_response(int sock) {
  int ret;
  int id;
  int command;

  ret=recv_rcon(sock, 1, &id, &command, string1, string2);
  if(DEBUG) printf("Received = %d : id=%d, command=%d, s1=%s, s2=%s\n",
    ret, id, command, string1, string2);
  if(ret==-1) {
    return -1;
  }

  switch(command) {
  case SERVERDATA_AUTH_RESPONSE:
    switch(id) {
    case 20:
      auth = 1;
      break;
    case -1:
      printf("Password Refused\n");
      return -1;
    default:
      printf("Bad Auth Response ID = %d\n",id);
      return -1 ;
    };
    break;

  case SERVERDATA_RESPONSE_VALUE:
    printf("%s",string1);
    break;
  default:
    printf("Unexpected command: %d",command);
    break;
  };

  return 0;
}
// end code to replace

bool runRconCMD(const char* ip, uint16_t port, const char* passwd, const char* cmd)
{
	struct sockaddr_in rcona;
	memset(&rcona,0, sizeof(rcona));

	rcona.sin_family = AF_INET;
	rcona.sin_addr.s_addr = inet_addr(ip);
	rcona.sin_port = htons(port);

	int rconsock = socket(AF_INET, SOCK_STREAM, 0);

	int ret = connect(rconsock, (struct sockaddr*) &rcona, sizeof(rcona));

	if (ret == -1)
	{
		std::cout << "Unable to connect to rcon port" << std::endl;
		return false;
	}

	auth = 0;

	ret = send_rcon(rconsock, 20, SERVERDATA_AUTH, passwd, "");

	if(ret == -1)
	{
	    perror("rcon: Sending password");
	    return false;
	};

	while(auth==0)
	{
	    if(process_response(rconsock)==-1) {
	      printf("rcon: couldn't authenticate\n");
	      return false;
	    }
	  }

	ret = send_rcon(rconsock, 20, SERVERDATA_EXECCOMMAND, cmd, "");

	if(ret == -1) {
		return false;
	}

	while(process_response(rconsock) != -1);

	close(rconsock);

	return true;
}


