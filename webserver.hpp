/*
 * webserver.hpp
 *
 *  Created on: 02.02.2019
 *      Author: sapier
 */
#include "switches.h"

#ifndef WEBSERVER_HPP_
#define WEBSERVER_HPP_

#ifdef HAVE_INTERNAL_WEBSERVER

#include <microhttpd.h>
#include <sstream>
#include <map>

class manager;

namespace Json {
	class Value;
}

class WebServer
{
	public:
		/**
		 * get pointer to webserver
		 * @return pointer
		 */
		static WebServer* getInstance();

		/**
		 * Handle incomming connection
		 * @param cls
		 * @param connection
		 * @param url
		 * @param method
		 * @param version
		 * @param upload_data
		 * @param upload_data_size
		 * @param con_cls
		 * @return
		 */
		int answerToConnection(void *cls, struct MHD_Connection *connection,
                          const char *url,
                          const char *method, const char *version,
                          const char *upload_data,
                          size_t *upload_data_size, void **con_cls);

	private:
		WebServer();

		/**
		 * read a marker file
		 * @param toset variable to store data to
		 * @param x grid x coordinate
		 * @param y grid y coordinate
		 */
		void readMarkerFile(Json::Value* toset, int x, int y);

		/**
		 * save a marker list to file
		 * @param toset data to write
		 * @param x grid x coordinate
		 * @param y grid y coordinate
		 */
		void saveMarkerFile(Json::Value* toset, int x, int y);

		/**
		 * Hanlde a form input from client
		 * @param url the url requested to handle the form
		 * @param postsize size of data provided
		 * @param postbuffer pointer to provided data
		 */
		void pageHandlerForm(const char* url, size_t postsize, const char* postbuffer);

		/**
		 * Provide the status page in old style
		 * @param outbuffer the buffer to put the page to
		 * @param servermanager pointer to servermanager
		 */
		void pageHandlerStatusOld(std::stringstream& outbuffer,	manager* servermanager);

		/**
		 * Provide the status page in json format
		 * @param outbuffer the buffer to put the page to
		 * @param servermanager pointer to servermanager
		 */
		void pageHandlerStatusJson(std::stringstream& outbuffer,	manager* servermanager);

		/**
		 * Provide images directly related to the servergrid
		 * @param url the requested url
		 * @param outbuffer the buffer to put the page to
		 * @param servermanager pointer to servermanager
		 */
		void pageHandlerServerGridImages(const char* url, std::stringstream& outbuffer,
				manager* servermanager);

		/**
		 * Provide webmap page
		 * @param url the requested webmap url
		 * @param outbuffer the the buffer to write the page to
		 */
		void pageHandlerWebMap(const char* url, std::stringstream& outbuffer);


		/**
		 * Provides files from the internal static file section
		 * @param url the requested url
		 * @param outbuffer the buffer to put the file to
		 */
		void pageHandlerStaticFiles(const char* url, std::stringstream& outbuffer);


		/**
		 * Provides "files" which are dynamicaly created from ridag internal data
		 * @param url the requested url
		 * @param outbuffer the buffer to put the file to
		 * @param servermanager pointer to main manager class
		 */
		void pageHandlerDynamicFiles(const char* url, std::stringstream& outbuffer,
				manager* servermanager);

		/**
		 * translate marker type to image name
		 * @param type
		 * @return image filename
		 */
		std::string getImageFromType(std::string type);


		/**
		 * load the marker image mappings
		 */
		void loadMarkerMappings();


		std::map<std::string, std::string> m_markerTypeImageLookupTable;
};


int answer_to_connection (void *cls, struct MHD_Connection *connection,
                          const char *url,
                          const char *method, const char *version,
                          const char *upload_data,
                          size_t *upload_data_size, void **con_cls);

#endif /* HAVE_INTERNAL_WEBSERVER */


#endif /* WEBSERVER_HPP_ */
