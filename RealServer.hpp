/*
 * RealServer.hpp
 *
 *  Created on: 27.01.2019
 *      Author: sapier
 */

#ifndef REALSERVER_HPP_
#define REALSERVER_HPP_
#include "iserver.hpp"
#include "common.hpp"
#include <hiredis/hiredis.h>
#include <string>
#include <map>

class RealServer : public IServer
{
public:

	RealServer(redisContext* context, std::string basepath, int x, int y );
	~RealServer();

	void runServer();

	int getActiveUsers();
	int timeSinceLastKnownAlive();
	unsigned int timeSinceLastPlayerActive();

	void setWANAddress(std::string param)
	{ m_parameters["WANAddress"] = param; }

	void setMaxPlayers(int param)
	{ m_parameters["MaxPlayers"] = std::to_string(param); }

	void setMaxReservedPlayers(int param)
	{ m_parameters["ReservedPlayerSlots"] = std::to_string(param); }

	void setStorageDirPrefix(std::string param)
	{ m_parameters["AltSaveDirectoryName"] = param + "X" +
		std::to_string(m_gridX) + "Y" + std::to_string(m_gridY); }

	void setServerPVE(bool param)
	{ m_parameters["serverPVE"] = param ? "True" : "False"; }

	int getRconPort();

	void setQueryPort(int port);

	void setSeamlessIp(std::string param)
	{ m_parameters["SeamlessIP"] = param; }

	int getPlayerCount()
	{
		if (m_ServerId.size() == 0)
			updateServerId();

		return readPlayerCount(m_context, m_ServerId);
	}

	void updateSysStats();

	float getCpuUsage()
	{ return m_cpuusage; }

	long unsigned int getMemoryUsage()
	{ return m_memoryusage; }

private:
	/* functions to set command line parameters */
	std::string buildCmdLineArgs();

	void updateServerId();

	int getTimeStamp();

	redisContext *m_context;
	std::string m_rediscluster;
	std::string m_ServerId;

	std::string m_basepath;
	int m_queryPort;

	std::string m_mapname;

	std::string m_binarypath;
	std::string m_appname;

	pid_t m_serverpid;

	int m_tsLastPlayerActive;
	int m_rconport;
	std::string m_rconpwd;

	long unsigned int m_totalusage;
	long unsigned int m_procusage;
	long unsigned int m_memoryusage;

	float m_cpuusage;

	std::map<std::string, std::string> m_parameters;
};

#endif /* REALSERVER_HPP_ */
