/*
 * common.cpp
 *
 *  Created on: 27.01.2019
 *      Author: sapier
 */

#include <string>
#include <hiredis/hiredis.h>

/*****************************************************************************/
std::string getClusterByPort(redisContext* context, int portnumber)
{
	std::string retval;
	redisReply *reply = (redisReply*) redisCommand(context, "KEYS cluster*");

	for (unsigned int j=0; j < reply->elements; j++)
	{
		redisReply *creply = (redisReply*) redisCommand(context, "HGET %s Port", reply->element[j]->str);

		if (creply)
		{
			int port = std::stoi(creply->str);
			freeReplyObject(creply);

			if (port == portnumber)
			{
				retval = reply->element[j]->str;
				break;
			}
		}
	}
	freeReplyObject(reply);

	return retval;
}

/*****************************************************************************/
int readPlayerCount(redisContext* context, std::string serverid)
{
	int playercount = 0;

	std::string retval;
	redisReply *reply = (redisReply*) redisCommand(context, "KEYS playerserverinfo*");

	if (!reply)
	{
		return -1;
	}

	for (unsigned int j=0; j < reply->elements; j++)
	{
		redisReply *creply = (redisReply*) redisCommand(context, "HGET %s CurrentServerId", reply->element[j]->str);

		if (creply)
		{
			if (std::string(creply->str) == serverid)
			{
				playercount++;
			}
			freeReplyObject(creply);
		}
		else {
			return -1;
		}
	}
	freeReplyObject(reply);
	return playercount;
}

/*****************************************************************************/
int readTimeSinceClusterLastOnline(redisContext* context, std::string atlasid, std::string clusterid)
{
	redisReply *reply = (redisReply*) redisCommand(context, "HGET cluster:%s:%s LastUpdated", atlasid.c_str(), clusterid.c_str());

	if ((reply) && (reply->str != 0))
	{
		return std::stod(reply->str);
	}

	return -1;
}

/*****************************************************************************/
void findAndReplace(std::string &tofix, std::string toreplace, std::string replaceby)
{
	size_t pos = tofix.find(toreplace);

	while( pos != std::string::npos)
	{
		tofix.replace(pos, toreplace.size(), replaceby);
		pos = tofix.find(toreplace, pos + toreplace.size());
	}
}
