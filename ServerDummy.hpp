/*
 * ServerDummy.hpp
 *
 *  Created on: 26.01.2019
 *      Author: sapier
 */

#ifndef SERVERDUMMY_HPP_
#define SERVERDUMMY_HPP_

#include <iostream>
#include <hiredis/hiredis.h>
#include "iserver.hpp"
#include "common.hpp"


class ServerDummy : public IServer
{
public:
	ServerDummy(redisContext* context, int cluster_port, int x, int y);
	~ServerDummy();

	bool publishServer(int available_servers);

	int getPlayerCount()
	{ return readPlayerCount(m_context, m_ServerId); }

	int getPort()
	{ return std::stod(m_Port); }

private:
	redisContext *m_context;

	std::string m_clusterid;
	std::string m_AtlasId;
	std::string m_ServerId;
	std::string m_Ip;
	std::string m_Port;
	std::string m_GameIp;
	std::string m_GamePort;

	int m_tsLastPublished;
	int m_statePublished;
};

#endif /* SERVERDUMMY_HPP_ */
